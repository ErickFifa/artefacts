package tapir.handlers;

import java.util.List;

public class Asset {
	String Patterns;
	String result;
	public int id;
	
	public Asset(int id, String p, String r) {
		this.id = id;
		this.Patterns = p;
		this.result = r;
	}
	public Asset(int id, String p, boolean r) {
		this.id = id;
		this.Patterns = p;
		this.addResult(r);
	}
	public String getId() {
		return Integer.toString(this.id);
	}
	
	public String getPattern() {
		return this.Patterns;
	}
	
	public String getResult() {
		return this.result;
	}
	
	public boolean isFalse() {
		return this.result.equals("Possible Violation Found");
	}
	public void addResult(boolean b) {
		if(b) {
			this.result = "True";
		}else {
			this.result = "Possible violation";
		}
	}
}
