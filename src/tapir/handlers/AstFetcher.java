package tapir.handlers;

import java.util.ArrayList;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;

/**
 * 
 * @author Erick Raelijohn
 * 
 */
public  class AstFetcher {
	IProject project;
	IPackageFragment[] packages;
	IWorkspaceRoot root;

	/**
	 * Class Constructor
	 */
	public  AstFetcher() {
		this.root = ResourcesPlugin.getWorkspace().getRoot();
		this.project = getProject();
		this.packages = getPackage();
//		this.initPackageManager();

	}

	private void initPackageManager() {

		for (IPackageFragment aPackage : this.packages) {
			try {
					if(aPackage.getKind()== IPackageFragmentRoot.K_SOURCE) {
						PackageManager pm = new PackageManager(aPackage);
						pm.construct();
					}
					

			} catch (Exception e) {
				System.err.println("Package Manager failed to exctract package");
				e.printStackTrace();
			}
		}
	}
	
	
	
	public ArrayList<PackageManager> getPackageManager(){
		ArrayList<PackageManager> list = new ArrayList<PackageManager>();
		for (IPackageFragment aPackage : this.packages) {
				try {
					if(aPackage.getKind()== IPackageFragmentRoot.K_SOURCE) {
						PackageManager pm = new PackageManager(aPackage);
//						System.out.println("PM name = " + aPackage.getElementName());
						list.add(pm);
					}
					

			} catch (Exception e) {
				System.err.println("Package Manager failed to exctract package");
				e.printStackTrace();
			}
			
		}
		return list;
	}
	
	public ArrayList<ICompilationUnit> getAllCompUnit(){
		ArrayList<ICompilationUnit> list = new ArrayList<ICompilationUnit>();
		for (IPackageFragment aPackage : this.packages) {
				try {
					if(aPackage.getKind()== IPackageFragmentRoot.K_SOURCE) {
						for(ICompilationUnit cu : aPackage.getCompilationUnits()){
							list.add(cu);
						}
						
					}
					

			} catch (Exception e) {
				System.err.println("Package Manager failed to exctract package");
				e.printStackTrace();
			}
			
		}
		return list;
	}
	

	/**
	 * 
	 * @return PackageFragment list
	 */
	public IPackageFragment[] getPackage() {
		IJavaProject javaProject = JavaCore.create(this.project);
		IPackageFragment[] packages = null ;
		try {
			if (this.project.isNatureEnabled("org.eclipse.jdt.core.javanature")) {
				packages = javaProject.getPackageFragments();
			}else {
				System.err.print("Project nature is not enabled");
			}

		} catch (JavaModelException e) {
			System.out.println("Error manipulation javaProject" + e.toString());

		} catch (CoreException e) {
			System.out.println("Java nature not enabled, details: " + e.toString());

		}

		return packages;
	}

	/**
	 * 
	 * @return project, it refers to index 0 because we made the assumption that
	 *         there should be only one project open at the execution of TAPIR
	 *         verification
	 */
	private IProject getProject() {
		IProject[] ip = this.root.getProjects();
		return ip[0];
	}

	/**
	 * @return project name as string
	 */
	public String getProjectName() {
		return this.project.getName();
	}

	

}
