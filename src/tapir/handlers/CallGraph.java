package tapir.handlers;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.MethodInvocation;

import tapir.handlers.CompUnitManager;

public class CallGraph {

//	public ArrayList<String> method = new ArrayList<String>();
	public List<Node> method = new ArrayList<Node>();
	public LinkedList<Integer> edges[];

	public CallGraph(CompUnitManager cu) {

		this.initEdges(20);
		this.getMethodList(cu);

	}
	

	public CallGraph(IMethod m) {
		this.initEdges(20);
		try {
			this.getMethodList(m,this.method);
		} catch (JavaModelException e) {
			
			e.printStackTrace();
		}
	}
	@SuppressWarnings("unchecked")
	private void initEdges(int size) {
		this.edges = new LinkedList[size];
		for (int i = 0; i < size; i++) {
			this.edges[i] = new LinkedList<>();
		}
	}

	private void getMethodList(CompUnitManager cu) {
		for (IType type : cu.getClassList()) {
			IMethod[] methods;
			try {
				methods = type.getMethods();
				for (IMethod method : methods) {
					this.method.add(new Node(0,method.getElementName()));
				}
			} catch (JavaModelException e) {

				e.printStackTrace();
			}

		}
	}
	private static String parserHelper(String sourc) {
		String[] tempMiddle = sourc.split("\n");
		String sourceStart = "public class Shell {";
		// add a fake class
		String sourceMiddle = "";

		for (String s : tempMiddle) {

			s = s.trim(); // removes extra spaces
			if (s.contains("=") && s.contains(".")) { // check if it's a declaration ; contains equal
				String sDuplicate = s.replaceAll(".+=", "") + " \n";
//				s = s + "\n" + sDuplicate;

			}
			if (s.trim().length() > 0 && !s.startsWith("---") && !s.startsWith("/") && !s.startsWith("*"))
				sourceMiddle += s.trim() + "\n";
		}
		String sourceEnd = "}";
		String source = sourceStart + sourceMiddle + sourceEnd;
		return source;
	}
	private void getMethodList(IMethod tm, List<Node> method2) throws JavaModelException {
		int nodeId = getID(method2.toString());
		String source = tm.getSource();
		ASTParser parser = ASTParser.newParser(AST.JLS11);
		parser.setSource(parserHelper(source).toCharArray());
		parser.setKind(ASTParser.K_COMPILATION_UNIT);
		final CompilationUnit cu = (CompilationUnit) parser.createAST(null);
		
		ASTVisitor astv = new ASTVisitor() {
//			int id = nodeId;
			int id = 0;
			public boolean visit(MethodInvocation node) {
				id++;
				if(node.getExpression() != null) {
					// Builder of className # method name pattern
					method2.add(new Node(id,node.getName().getFullyQualifiedName()+"#"+node.getExpression()));
					
				}else {
					method2.add(new Node(id,node.getName().getFullyQualifiedName()));
				}
				
				return true;
			}
		};
		cu.accept(astv);
	}

	public ArrayList<String> getMethod() {
		// TODO get all Label of each node
		ArrayList<String> methodList = new ArrayList<String>();
		for( Node n : this.method) {
			methodList.add(n.getLabel());
		}
		return methodList;
	}
	public void printGraph() {
		printAllNode();
		printAllEdge();
	}


	private void printAllEdge() {
		
		for (int v = 0; v < this.edges.length; v++) {

			System.out.print(printEdge(v));

		}
	}


	private String printEdge(int id) {
		String res = "";
		for (Integer pCrawl : this.edges[id]) {

			res += Integer.toString(id) + "->"+ Integer.toString(pCrawl)+ "\n";
		}
		return res;
	}


	private void printAllNode() {
		for (int v = 0; v < this.method.size(); v++) {
			if (printNode(v) != null) {
				System.out.print(printNode(v));
			}
		}
		
	}


	private String printNode(int id) {
		String res = "";

		res += Integer.toString(id)+ " [label = "+ this.method.get(id).label+ " ]"+ "\n";
		return res;
	}
	
	/**
	 * Create a hash of the method name 
	 * @param: method Name
	 * @return an unique ID
	 */
	private int getID(String s) {
	    final int prime = 5;
	    int result = 1;
	    result = prime * result + ((s == null) ? 0 : s.hashCode());
	    return result;
	}
	
}
