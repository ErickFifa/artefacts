package tapir.handlers;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.JavaModelException;

import tapir.views.Configuration;

public class CallGraphManager {
	Map<String, CallGraph> fileCgs;
	Map<String, CallGraph> methodCgs;
	// map the class name to a map of the method - graphs
	Map<String, String> dictionary;
	// map a method to a graph
	Map<String, Graph> methodGraph;
	AstFetcher ast;
	ArrayList<PackageManager> pack;
	ArrayList<CompUnitManager> cu;
	ArrayList<Asset> assets;
	ArrayList<String> specs;
	ModelChecker modelChecker;
	
	public CallGraphManager() {
		this.fileCgs = new HashMap<String, CallGraph>();
		this.methodCgs = new HashMap<String, CallGraph>();
		this.dictionary = new HashMap<String, String>();
		this.methodGraph = new HashMap<String, Graph>();
		this.ast = new AstFetcher();
		this.pack = ast.getPackageManager();
		this.cu = pack.get(0).getCompUnit();
		this.crawl();
		try {
			this.buildDictionary();
			this.specs = getSpecs();
			this.modelChecker = new ModelChecker();
			this.assets = this.getAssets();
		} catch (JavaModelException e) {
			e.printStackTrace();
		}
		
		
		
	}

	private ArrayList<Asset> getAssets() {
		// loop through the spec and build the assets
				ArrayList<Asset> list = new ArrayList<Asset>();
				int id = 0;
				System.out.println("This specs element:" + this.specs);
				for (String spec : this.specs) {
					System.out.println("for inter:-=-=-=-=-=-=-=-=-");
					String answer  = new String();
					try {
						answer = this.modelCheck(spec);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					list.add(new Asset(id, spec, answer));
					id++;
				}

				return list;
		
	}
	
	public ArrayList<Asset> getViewElement() {
		return this.assets;
	}
	private String modelCheck( String spec) throws IOException {
		// write the file (graph ) as input
		//Get Path to temporary folder from the configuration in config.tapriconf[0]
//		String temporaryFolder = Configuration.tapirConfiguration[0];
		String temporaryFolder = Config.TEMPFOLDER();
		File file = new File(temporaryFolder + "\\graph.smv");
//		System.out.println("file location: " + System.getProperty("user.dir") + "\\resources\\" + "graph.smv");
		try {
			String smvModel = "";
			for (String str : this.buildDeepGraph()) {
				smvModel += str + "\n";
			}
			// add the pattern inside the smv model
			smvModel += spec;
			System.out.println("---This is Model:::: " + smvModel);
			BufferedWriter writer = new BufferedWriter(new FileWriter(file));
			writer.write(smvModel);
			writer.close();
//			System.out.println("args: "+ this.modelCheker.args);
			this.modelChecker.initModel();
//			System.out.println("---This is output in wrapper:::: " + this.modelCheker.output);
			return this.modelChecker.getAnswer();

		} catch (Exception e) {
			System.err.print("Error runing the model Checker program");
			e.printStackTrace();
		}
		return "Undefined";
	}
	public ArrayList<String> getSpecs() {
		try {
//			String ltlFile = Configuration.tapirConfiguration[2];
			String ltlFile = Config.LTLFILE();
			System.out.println("Ltl File: " +ltlFile);
			return new LtlParser(new FileReader(ltlFile))
					.getSpecList();
		} catch (Exception e) {
			System.err.print("Spec File not Found");
			e.printStackTrace();
		}
		return null;
	}
	public void crawl() {
		for (CompUnitManager javaFile : this.cu) {
			String fileName = javaFile.cUnit.getElementName();
			fileCgs.put(fileName, new CallGraph(javaFile));
			ArrayList<TypeManager> tms = javaFile.getTypeManager();
			for (TypeManager tm : tms) {
				for (IMethod m : tm.methodList) {
					this.methodCgs.put(m.getElementName(), new CallGraph(m));
				}

			}
		}

	}

	/**
	 * Provide a High level view of the methods inside each class (file)
	 */
	public void viewMethod() {
		for (Map.Entry<String, CallGraph> entry : this.fileCgs.entrySet()) {
			System.out.println("File Name = " + entry.getKey());
			System.out.println("Method inside = " + this.getInsideMethod(entry.getKey()));

		}
	}

	public void viewInsideMethod() {
		for (Map.Entry<String, CallGraph> mName : this.methodCgs.entrySet()) {
			System.out.println("method name: " + mName.getKey());
			System.out.println("Content=: " + mName.getValue().getMethod());
		}
	}
	

	public ArrayList<String> getInsideMethod(String className) {
		// get all method of that className.

		ArrayList<String> method = new ArrayList<String>();
		if (!this.fileCgs.containsKey(className))
			return null;
		return method = this.fileCgs.get(className).getMethod();

	}

	/**
	 * 
	 * @param className
	 * @return
	 */
	public ArrayList<String> getCall(String className) {
		// get all method of that className.

		if (!this.methodCgs.containsKey(className))
			return null;
		return this.methodCgs.get(className).getMethod();

	}

	public void buildDictionary() throws JavaModelException {
		String className = new String();
		String methodName = new String();
		// loop for each class
		for (CompUnitManager cu : this.cu) {
			className = cu.cUnit.getElementName();
//			System.out.println("className= "+ className);
			ArrayList<TypeManager> tm = cu.getTypeManager();
			ArrayList<MethodManager> mm = tm.get(0).getMethodManager();
			for (MethodManager m : mm) {
				methodName = m.getName();
//				System.out.println("methodName= "+ methodName);
				Graph g = this.unfoldMethod(className, methodName);
				Graph empty = new Graph();
				try {
					this.methodGraph.put(methodName, g);
					this.dictionary.put(className, methodName);
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
//			System.out.println("--------------------");
		}
	}

	/**
	 * build the graph of a method inside the given class
	 * 
	 * @param className
	 * @param methodName
	 * @return
	 * @throws JavaModelException
	 */
	public Graph unfoldMethod(String className, String methodName) throws JavaModelException {
		int cIndex = classindex(this.cu, className);

		ArrayList<TypeManager> tm = this.cu.get(cIndex).getTypeManager();
		ArrayList<MethodManager> mm = tm.get(0).getMethodManager();
		int mIndex = methodIndex(mm, methodName);

		Graph g = new Graph(mm.get(mIndex).getSource(),methodName);

		g.printGraph();
		return g;
	}

	public ArrayList<String> viewDeepGraph() {
//		System.out.println(this.fileCgs.keySet());
		ArrayList<String> output = new ArrayList<String>();
		ArrayList<String> invokation = this.getCall("main");
		String temp = "";
		output.add("edge [color = blue] ");
		output.add("Entrypoint ->");
		for (String s : invokation) {

			if (s.contains("#"))
				s = s.substring(s.indexOf("#") + 1, s.length());

			output.add(s + "\n");
			// print for each method call
			output.add(s + " ->");
			String[] insideCall = this.methodGraph.get(s).getAllNode(1).split("\n");

			for (String call : insideCall) {

				if (!call.contains("EntryPoint") & !call.contains("ExitPoint")) {
//					if(its a predicate)
					if (call.contains("IF")) {
						// get last call
						// wait to endif
						// add an arrow to what's coming after endif
						temp = output.get(output.size() - 2) + "->";

						continue;
					}

					output.add(call);
					output.add("->");

				}

			}

		}
		output.add("ExitPoint");
		for (String c : output) {
			System.out.print(c);
		}
		return output;

	}
	public ArrayList<String> buildDeepGraph(){
		String entryPoint = "Main#main";
		ArrayList<String> output = new ArrayList<String>();
		Map<String, Integer> list = new HashMap<String, Integer>();
		output.add("MODULE main\r\n" + "VAR\r\n");
		// Writes all the method with the predicates
		output.add("method:{");
//		output.add("EntryPoint,");
		Iterator<String> it = this.methodGraph.keySet().iterator();
		int val = 0;
		String t = new String();
		while(it.hasNext()) {
			val++;
			for(String s : this.methodGraph.get(it.next()).returnAllNode()) {
				if(!s.strip().equals("ExitPoint_Main#main")) {
					
					list.put(s.strip()+",", val);
				}
				
			}
			
		}
			
				
		
		it = list.keySet().iterator();
		String temp = new String();
		while(it.hasNext()) {
			temp+= it.next();
		
		}
		temp = temp.substring(0, temp.length()-1);
		output.add(temp+", ExitPoint};");
		list.clear();
		// ===========================================
		output.add("ASSIGN \r\n" + 
				"init(method):= EntryPoint;");
		output.add("next(method) := case ");
		// Print edges of each graph 
		Iterator<String> nit = this.methodGraph.keySet().iterator();
		while(nit.hasNext()) {
			t = nit.next();
//		
			if(t.equals("Main#main")) {
				output.add(this.methodGraph.get("Main#main").returnEntryPointEdge());
			}else {
				output.add(this.methodGraph.get(t).returnAllEdgeSmv(t));
			}
		}
//		output.add(this.methodGraph.get("A#one").returnAllEdgeSmv("A#one"));
//		output.add(this.methodGraph.get("A#two").returnAllEdgeSmv("A#two"));
//		output.add(this.methodGraph.get("B#three").returnAllEdgeSmv("B#three"));
//		output.add(this.methodGraph.get("A#four").returnAllEdgeSmv("A#four"));
//		output.add(this.methodGraph.get("API#open").returnAllEdgeSmv("API#open"));
//		output.add(this.methodGraph.get("API#close").returnAllEdgeSmv("API#close"));
		// ================
		output.add("(method = ExitPoint) : {ExitPoint }; \r\n" + 
				"TRUE : method;\r\n" + 
				"esac;");
		
		return output;
	}
	public ArrayList<String> buildDeepGraph(String entryPoint) {
		//TODO get a list off all the elements and put it in a list ( -class, -method, -edges?)
		// List to push all result 
		Map<String, String> highLevelGraph =  new HashMap<String, String>();
		Map<String, String> lowLevelGraph =  new HashMap<String, String>();
		Map<String, Integer> methodList = new HashMap<String, Integer>();
		ArrayList<String> output = new ArrayList<String>();
		String result = "entryPoint -> ";
		System.out.println("++++ Output for buildDeepGraph +++++++");
//		viewMethod();
		
//		System.out.println("entry Point Content: "+ this.fileCgs.get(entryPoint).getMethod());
//		System.out.println("entry Point Content: " + this.methodCgs.get("main").getMethod());
		System.out.println("@=======");
		this.methodGraph.get("main#Main").printGraph("main");
				System.out.println("@=======");
		System.out.println("@=======");
		this.methodGraph.get("two#A").printGraph("two");
		System.out.println("@=======");
		// output all content 
		int id = 0;
		for(String s :this.methodCgs.get("main").getMethod()){
			
			id++;
			highLevelGraph.put(String.valueOf(id), s);
		}
		
//		System.out.println(highLevelGraph);
		//for each method , get inside call 
		for(Map.Entry<String, String> entry : highLevelGraph.entrySet()) {
//			lowLevelGraph.put(key, value) put(0+id,
			 id = 0;
			for(String ss : this.methodCgs.get(entry.getValue().substring(entry.getValue().indexOf("#")+1)).getMethod()) {
				String t_id = entry.getKey() + "_" + id;
				lowLevelGraph.put(t_id, ss);
				id++;
			}
//			System.out.println("method id: " + entry.getKey());
//			System.out.println("Content=: " + this.methodCgs.get(entry.getValue().substring(entry.getValue().indexOf("#")+1)).getMethod());
//			System.out.println("Content=: " + entry.getValue().substring(entry.getValue().indexOf("#")+1));
		}
//		System.out.println(lowLevelGraph);
		// Printing out the method list. 
		Iterator<String> itr = highLevelGraph.values().iterator();
		int temp = 0;
		while (itr.hasNext()) {
			methodList.put(itr.next(), temp );
			temp++;
			
		}
		itr = lowLevelGraph.values().iterator();
		while (itr.hasNext()) {
			methodList.put(itr.next(), temp );
			temp++;
			
		}
		//write the list of method
		output.add("MODULE main\r\n" + 
				"VAR\r\n");
		output.add("method:{");
		output.add("EntryPoint,");
		itr = methodList.keySet().iterator();
		//TODO add new if with ID 
		while(itr.hasNext()) {
			output.add(itr.next() + ",");
		}
		output.add("ExitPoint};");
		
		//----------
		output.add("ASSIGN \r\n" + 
					"init(method):= EntryPoint;");
		output.add("next(method) := case ");
		//--- Write all high level graph values
		itr = highLevelGraph.values().iterator();
		ArrayList<String> t = new ArrayList<String>();
		while(itr.hasNext()) {
			t.add(itr.next());
		}
		output.add("(method = EntryPoint) : {"+ t.get(0)+ "};" );
//		for(int i=1 ; i < t.size() ; i++) {
//			output.add("(method =" + t.get(i-1) + ") : {" + t.get(i) + "};" );
//		}
//		output.add("(method = " + t.get(t.size()-1) + ") : {ExitPoint };");
		//----------
//		output.add(this.methodGraph.get("main#Main").returnAllEdgeSmv());
		// --- Write low Level graph method
		t.clear();
		for(Map.Entry<String, String> entry : highLevelGraph.entrySet()) {
			String res = new String();
			for(Map.Entry<String, String> entry2 : lowLevelGraph.entrySet()) {
				
				if(entry2.getKey().startsWith(entry.getKey())) {
					res += entry2.getValue() + ",";
				}
			}
			if(!res.isEmpty()) {
				res = res.replaceFirst(".$","");
				output.add("(method = " +entry.getValue() +") : {" +
						res + "}; \r\n");
				res = new String();
			}
			
			
		}
		//---------
		output.add("(method = ExitPoint) : {ExitPoint }; \r\n" + 
				"TRUE : method;\r\n" + 
				"esac;");
		return (output);
	}

	private int methodIndex(ArrayList<MethodManager> mm, String methodName) {
		int i = -1;
		for (MethodManager m : mm) {

			if (m.getName().equals(methodName)) {
				i = mm.indexOf(m);
			}
		}
		return i;
	}

	private int classindex(ArrayList<CompUnitManager> cm, String className) {
		int i = -1;

		for (CompUnitManager c : cm) {
			if (c.cUnit.getElementName().equals(className)) {
				i = cm.indexOf(c);
			}
		}
		return i;
	}

	public void drawGraph(String entryPoint) {
//		String temporaryFolder = Configuration.tapirConfiguration[0];
		String temporaryFolder = Config.TEMPFOLDER();
		File file = new File(temporaryFolder + "\\graph.dot");
		ArrayList<String> builder = new ArrayList<String>();
		builder.add("digraph{");

		// Draw the class
		String[] classList = this.fileCgs.keySet().toArray(new String[0]);
		int cluster = 0;
		for (String s : classList) {

			builder.add("subgraph cluster_" + cluster++ + "  { ");
			builder.add("label = \"" + s.substring(0, s.indexOf(".")) + "\" ;");

			String temp = new String();
			for (String e : this.getInsideMethod(s)) {
				temp += e + ";";
			}
			builder.add(temp);
			builder.add("}");
		}

		// Draw sense according from the entryPoint
//		ArrayList<String> invokation = this.getCall(entryPoint);
//		String method = "Entrypoint ->";
//		for (String s : invokation) {
//			if (s.contains("#"))
//				s = s.substring(s.indexOf("#") + 1, s.length());
//			method += s + " -> ";
//		}
//		
//		method += "Exitpoint";
//		builder.add(method);
		builder.addAll(this.viewDeepGraph());
		builder.add("}");
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(file));
			for (String s : builder) {

				writer.write(s + "\n");
			}

			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void renderGraph() throws IOException, InterruptedException {
		ArrayList<String> args = new ArrayList<String>();
		if (System.getProperty("os.name").toLowerCase().contains("win")) {
			args.add("cmd.exe");
			args.add("/c");
			args.add("dot.exe");
			args.add("-Tpng");
			args.add("E:\\Eclipse-workspace\\TapirPluginDev\\temp\\graph.dot");
			args.add("-o");
			args.add("E:\\Eclipse-workspace\\TapirPluginDev\\temp\\graph.png");
		}
		ProcessBuilder build = new ProcessBuilder();
		build.command(args);
		build.directory();
		Process proc = build.start();
		System.out.println("exit code:" + proc.waitFor());
		this.getErrOutput(proc);
	}

	private void getErrOutput(Process proc) {
		String line;
		BufferedReader input = new BufferedReader(new InputStreamReader(proc.getErrorStream()));
		try {
			while ((line = input.readLine()) != null) {
				System.out.println(line);
			}

		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	public void refreshGraph() {
		this.fileCgs = new HashMap<String, CallGraph>();
		this.methodCgs = new HashMap<String, CallGraph>();
		this.dictionary = new HashMap<String, String>();
		this.methodGraph = new HashMap<String, Graph>();
		this.ast = new AstFetcher();
		this.pack = ast.getPackageManager();
		this.cu = pack.get(0).getCompUnit();
		this.crawl();
		this.specs = getSpecs();
		this.modelChecker = new ModelChecker();
		this.assets = this.getAssets();
		try {
			this.buildDictionary();
		} catch (JavaModelException e) {
			e.printStackTrace();
		}
	}

}
