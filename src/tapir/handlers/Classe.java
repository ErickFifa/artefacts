package tapir.handlers;

import java.util.ArrayList;

import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.MethodDeclaration;

public class Classe {
	String name;
	String source;
	ArrayList<Method> methods;
	public Classe(String name,String source) {
		this.name =name;
		this.source = source;
		this.methods = this.fetchMethodList();
		
	}
	
	/**
	 * 
	 * @return all the method declaration inside a class file
	 */
	private ArrayList<Method> fetchMethodList() {
		ArrayList<Method> mList = new ArrayList<Method>();
		String thisclassName = this.name;
		@SuppressWarnings("deprecation")
		ASTParser parser = ASTParser.newParser(AST.JLS11);
		parser.setSource(parserHelper(this.source).toCharArray());
		parser.setKind(ASTParser.K_COMPILATION_UNIT);
		final CompilationUnit cu = (CompilationUnit) parser.createAST(null);
		ASTVisitor astv = new ASTVisitor() {
			public boolean visit(MethodDeclaration node) {
				
					if(node.getBody() == null) {
						mList.add(new Method(thisclassName+"#"+node.getName().toString(),""));
						return true;
					}
					
					mList.add(new Method(thisclassName+"#"+node.getName().toString(),node.getBody().toString()));
				return true;
			}
		};
		cu.accept(astv);
		return mList;
		
		
		
	}
	
	/**
	 * Format the source to be accepted as a valid format by the ASTParser
	 * @param sourc
	 * @return
	 */
	private static String parserHelper(String sourc) {
		String[] tempMiddle = sourc.split("\n");
		String sourceStart = "public class Shell {";
		// add a fake class
		String sourceMiddle = "";

		for (String s : tempMiddle) {

			s = s.trim(); // removes extra spaces
			if (s.contains("=") && s.contains(".")) { // check if it's a declaration ; contains equal
//				String sDuplicate = s.replaceAll(".+=", "") + " \n";
//				s = s + "\n" + sDuplicate;

			}
			if (s.trim().length() > 0 && !s.startsWith("---") && !s.startsWith("/") && !s.startsWith("*"))
				sourceMiddle += s.trim() + "\n";
		}
		String sourceEnd = "}";
		String source = sourceStart + sourceMiddle + sourceEnd;
		return source;
	}
	
	
	
	
	public String getName() {
		return this.name;
	}
	
	public String getSource() {
		return source;
	}
	
	public ArrayList<String> getMethodList() {
		ArrayList<String> result = new ArrayList<String>();
		for(Method m : this.methods) {
			result.add(m.getName());
		}
		return result;
	}
	/**
	 * 
	 * @return a list of a list of method invocation inside each method of the class
	 */
	public ArrayList<ArrayList<String>> getInsideMethodInvocation() {
		ArrayList<ArrayList<String>> result = new ArrayList<ArrayList<String>>();
		for(Method m : this.methods) {
			result.add(m.getMethodInvocationList());
		}
		return result;
	}
	
	
	
	
	
}
