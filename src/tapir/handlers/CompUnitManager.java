package tapir.handlers;

import java.util.ArrayList;

import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaModelException;

public class CompUnitManager {
	ICompilationUnit cUnit;
	String source;
	IType[] classList;

	public CompUnitManager(String packageName, ICompilationUnit unit) {
		this.source = packageName;
		this.cUnit = unit;
		this.classList = this.getMethod();
		
	}
	
	public IType[] getMethod() {
		IType[] it = null;
		try {
			
				it = this.cUnit.getAllTypes();
						
		} catch (JavaModelException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return it;
	}
	
	public void initMethodClass() throws JavaModelException {
		for(IType type : this.getClassList()) {
			IMethod[] methods = type.getMethods();
			TypeManager tm = new TypeManager(methods,this.cUnit.getElementName());
			
		}
	}
	
	public ArrayList<TypeManager> getTypeManager()  {
		ArrayList<TypeManager> list = new ArrayList<TypeManager>();
		try {
			for(IType type : this.getClassList()) {
				IMethod[] methods = type.getMethods();
				TypeManager tm = new TypeManager(methods,this.cUnit.getElementName());
				list.add(tm);
			}	
		}catch(Exception e) {
			System.err.print("Type Manager Error.");
		}
		
		return list;
	}

	public IType[] getClassList() {
		return classList;
	}
}
