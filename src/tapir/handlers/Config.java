package tapir.handlers;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Config {
	static String TEMPFOLDER = new String();
	static String LTLFILE = new String();
	static String APIFILE = new String();
	static ArrayList<String> APIMETHOD = new ArrayList<String>();
	
	public static String TEMPFOLDER() {
		return "C:\\Users\\Erick\\Documents\\tapirecommender\\temp";
	}
	public static String LTLFILE() {
		return "C:\\Users\\Erick\\Documents\\validationutils\\data\\Map.ltl";
	}
	
	public static ArrayList<String> APIMETHOD(){
		return APIMETHOD;
	}
	
	public static void SetLTLFILE(String s) {
		LTLFILE = s;
	}
	
	public static void setTEMPFOLDER(String s){
		TEMPFOLDER = s;
	}
	
	public static void setAPIList(String s){
		APIFILE = s;
		APIMETHOD.add("API#close");
		APIMETHOD.add("API#open");
//		getAPIMethodList(s);
	}
	
	private static void getAPIMethodList(String s) {
		try {
			   File file = new File(s);
			   Scanner scanner = new Scanner(file);
			   while (scanner.hasNext()) {
			   APIMETHOD.add(scanner.next());
			   }
			   scanner.close();
			  } catch (FileNotFoundException e) {
			   e.printStackTrace();
			  } 
	}
	
	public static boolean isSet() {
//		return !TEMPFOLDER.isEmpty();
		return true;
	}

}
