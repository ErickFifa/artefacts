package tapir.handlers;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.jgrapht.traverse.*;
import org.jgrapht.Graphs;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.io.*;

@SuppressWarnings("deprecation")
public class DeepGraph {
	String entryPoint;
	List<Classe> classList;
	Map<String, DefaultDirectedGraph<String, Scope>> graphList;
	Map<String, DefaultDirectedGraph<String, Scope>> headlessGraphList = new HashMap<String, DefaultDirectedGraph<String, Scope>>();
	DefaultDirectedGraph<String, Scope> graph = new DefaultDirectedGraph<>(Scope.class);

	protected DeepGraph(List<Classe> classeList, String classe, String method) {

		this.classList = classeList;
		this.entryPoint = classe + "#" + method;
		this.graphList = this.fetchGraph();
		this.headlessGraphList.putAll(this.graphList);
//		this.removehead();
		this.graph = this.unfoldGraph();
		this.exportGraph();
//		this.addGraph();
	}

	public DefaultDirectedGraph<String, Scope> getGraph() {
		return this.graph;
	}

	/**
	 * Fetch all the graph of the project and put them in a list
	 */
	private Map<String, DefaultDirectedGraph<String, Scope>> fetchGraph() {
		Map<String, DefaultDirectedGraph<String, Scope>> graphLists = new HashMap<String, DefaultDirectedGraph<String, Scope>>();
		for (Classe c : this.classList) {
			for (Method m : c.methods) {
				graphLists.put(m.getName(), m.directedGraph);
//				Graphs.addGraph(this.graph,m.directedGraph);
			}
		}
		return graphLists;
	}

	/**
	 * link all known method declaration to its graph
	 */
	private DefaultDirectedGraph<String, Scope> unfoldGraph() {
		DefaultDirectedGraph<String, Scope> result = new DefaultDirectedGraph<>(Scope.class);
		Iterator<String> temp = this.graphList.keySet().iterator();
		ArrayList<Scope> edgeToRemove = new ArrayList<Scope>();
		while (temp.hasNext()) {
			String selectGraph = temp.next();
			Graphs.addGraph(result, this.graphList.get(selectGraph));
			DefaultDirectedGraph<String, Scope> SelectedGraph = this.graphList.get(selectGraph);
			Iterator<String> iter = this.headlessGraphList.keySet().iterator();
			while (iter.hasNext()) {
				String v = iter.next();
				ArrayList<String> t = new ArrayList<String>();
				DefaultDirectedGraph<String, Scope> toUnfold;
				if (!v.equals(selectGraph)) {
					if (SelectedGraph.containsVertex(v)) {
						toUnfold = this.graphList.get(v);
						Graphs.addGraph(result, toUnfold);
						Set<Scope> ts = result.outgoingEdgesOf(v);
						for (Scope s : ts) {
							if (s.getScope().equals(selectGraph)) {
								if (SelectedGraph.containsEdge(s)) {
									t.add(s.target());
									edgeToRemove.add(s);
								}

							}
						}
						if (!t.isEmpty()) {
							if (t.size() > 2)
							result.addEdge("Exit" + v, t.get(0), new Scope(selectGraph));
						} else {
						}
						// doesnt contains vertex
					}
				}

			}
		}
		for (Scope s : edgeToRemove) {

			String targ = s.target();
			Set<Scope> targIncomingEdge = result.incomingEdgesOf(targ);
//			System.out.println("SIZE =" +targIncomingEdge.size());
			if (targIncomingEdge.size() >= 1) {
				if (s.target().contains("Exit")) {
//					System.out.println("removin edge from= " + s.source() + " to " + s.target());
					result.removeEdge(s);
				}
			}

		}

		return result;

	}

	private void addGraph() {
		Iterator<String> temp = this.graphList.keySet().iterator();
		while (temp.hasNext()) {
			Graphs.addGraph(this.graph, this.graphList.get(temp.next()));
		}

	}

	private void removehead() {
		Iterator<String> it = this.headlessGraphList.keySet().iterator();
		while (it.hasNext()) {
			String selectedNode = it.next();
			DefaultDirectedGraph<String, Scope> graph = this.headlessGraphList.get(selectedNode);
			String start = graph.vertexSet().stream().filter(string -> string.equals(selectedNode)).findAny().get();
			Iterator<String> iter = new DepthFirstIterator<>(graph, start);
			graph.removeVertex(iter.next());
		}
	}

	/**
	 * 
	 * @param graph1
	 * @param graph2
	 * @param whereToAdd
	 * @param scope
	 */
	private static void addEdgetoGraph(DefaultDirectedGraph<String, Scope> graph1, String node,
			DefaultDirectedGraph<String, Scope> graph, String scope) {
		Graphs.addGraph(graph1, graph);
		String lastNode = new String();
		Iterator<String> it = graph.vertexSet().iterator();
		String firstElem = it.next();
		it = graph.vertexSet().iterator();
		while (it.hasNext()) {
			lastNode = it.next();
		}
		// first elem of graph
		graph1.addEdge(node, firstElem, new Scope(scope));

		// target
		String target = new String();
		Set<Scope> conflictNodeTarget = graph1.outgoingEdgesOf(node);
		Iterator<Scope> iter = conflictNodeTarget.iterator();
		while (iter.hasNext()) {
			target = iter.next().target();
		}
		graph1.addEdge(lastNode, target, new Scope(scope));
		graph1.removeAllEdges(node, target);

	}

	public void showDeepGraph() {
		String result = new String();
//		for(DefaultDirectedGraph<String, DefaultEdge> d : this.graphList) {
//			result += d.toString() + "\n";
//		}
		System.out.print(result);
	}

	/**
	 * Export the graph in dot syntax with the following file format:
	 * tempFolder//ClassName#MethodName.dot
	 */
	@SuppressWarnings("deprecation")
	public void exportGraph() {
		FileWriter writer;
		try {
			writer = new FileWriter(Config.TEMPFOLDER() + "\\" + "deepGraph" + ".dot");
			BufferedWriter bwr = new BufferedWriter(writer);

			DOTExporter<String, Scope> exporter = new DOTExporter<String, Scope>(
					new IntegerComponentNameProvider<String>(), // vertex ids
					new StringComponentNameProvider<String>(), // vertex labels
					new StringComponentNameProvider<Scope>(), // edge labels
					null, // vertex attributes
					null); // edge attributes);
			exporter.exportGraph(this.graph, bwr);
		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	/**
	 * 
	 * @param list of API method
	 */
	public void exportGraph(List<String> methodList) {
		FileWriter writer;

		try {
			writer = new FileWriter(Config.TEMPFOLDER() + "\\" + "deepGraph" + ".dot");
			BufferedWriter bwr = new BufferedWriter(writer);
			ComponentAttributeProvider<String> cap = new ComponentAttributeProvider<>() {
				public Map<String, Attribute> getComponentAttributes(String e) {
					Map<String, Attribute> map = new HashMap<String, Attribute>();
					// if it;s the list change the form to
					Attribute st = new Attribute() {

						@Override
						public AttributeType getType() {
							return null;
						}

						@Override
						public String getValue() {
							return "filled";
						}

					};
					Attribute stc = new Attribute() {

						@Override
						public AttributeType getType() {
							return null;
						}

						@Override
						public String getValue() {
							return "blue";
						}

					};

					if (methodList.contains(e)) {
						map.put("color", stc);
						map.put("Style", st);
					}

					return map;
				}
			};
			DOTExporter<String, Scope> exporter = new DOTExporter<String, Scope>(
					new IntegerComponentNameProvider<String>(), // vertex ids
					new StringComponentNameProvider<String>(), // vertex labels
					new StringComponentNameProvider<Scope>(), // edge labels
					cap, // vertex attributes
					null); // edge attributes);
			exporter.exportGraph(this.graph, bwr);
		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	/**
	 * Export the graph in dot syntax with the following file format:
	 * tempFolder//ClassName#MethodName.dot
	 */
	@SuppressWarnings("deprecation")
	public static void exportGraph(String id, DefaultDirectedGraph<String, Scope> graph, ArrayList<String> instruct) {
		FileWriter writer;
		try {
			writer = new FileWriter(Config.TEMPFOLDER() + "\\" + id + ".dot");
			BufferedWriter bwr = new BufferedWriter(writer);
			ComponentAttributeProvider<Scope> cap = new ComponentAttributeProvider<>() {
				public Map<String, Attribute> getComponentAttributes(Scope e) {
					Map<String, Attribute> map = new HashMap<String, Attribute>();
					// if it's in a list then add red color
					Attribute at = new Attribute() {

						@Override
						public AttributeType getType() {
							return null;
						}

						@Override
						public String getValue() {
							return "red";
						}

					};

					if (instruct.contains(e.source())) {
						int s = instruct.indexOf(e.source());
						int d = s + 1;
						if (instruct.get(d).equals(e.target()))
							map.put("color", at);

					}

					return map;
				}

			};
			DOTExporter<String, Scope> exporter = new DOTExporter<String, Scope>(
					new IntegerComponentNameProvider<String>(), // vertex ids
					new StringComponentNameProvider<String>(), // vertex labels
					new StringComponentNameProvider<Scope>(), // edge labels
					null, // vertex attributes
					cap); // edge attributes);

			exporter.exportGraph(graph, bwr);
		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	public DefaultDirectedGraph<String, Scope> counterExample(ArrayList<String> instruct) {
		ArrayList<String> concernedMethod = new ArrayList<String>();
		for (String in : instruct) {
			if (this.graphList.containsKey(in))

				concernedMethod.add(in);
		}
		DefaultDirectedGraph<String, Scope> result = new DefaultDirectedGraph<>(Scope.class);
		ArrayList<Scope> edgeToRemove = new ArrayList<Scope>();
		for (String state : concernedMethod) {
			String selectGraph = state;
			Graphs.addGraph(result, this.graphList.get(selectGraph));
			DefaultDirectedGraph<String, Scope> SelectedGraph = this.graphList.get(selectGraph);
			Iterator<String> iter = this.headlessGraphList.keySet().iterator();
			while (iter.hasNext()) {
				String v = iter.next();
				ArrayList<String> t = new ArrayList<String>();
				DefaultDirectedGraph<String, Scope> toUnfold;
				if (!v.equals(selectGraph)) {
					if (SelectedGraph.containsVertex(v)) {
						toUnfold = this.graphList.get(v);
						Graphs.addGraph(result, toUnfold);
						Set<Scope> ts = result.outgoingEdgesOf(v);
						for (Scope s : ts) {
							if (s.getScope().equals(selectGraph)) {
								t.add(s.target());
								edgeToRemove.add(s);
							}
						}
						if (!t.isEmpty()) {

							result.addEdge("Exit" + v, t.get(0), new Scope(selectGraph));
						}

					}
				}

			}
		}
		for (Scope s : edgeToRemove) {
			result.removeEdge(s);
		}
		return result;
	}
}
