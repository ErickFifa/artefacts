package tapir.handlers;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class GetPropertyValues {
	InputStream is;
	List<String> values;
	
	public GetPropertyValues(String fileName) {
		this.is = getClass().getClassLoader().getResourceAsStream(fileName);
		this.values= new ArrayList<String>();
		try {
			this.getPropValues();
		} catch (IOException e) {
			System.err.print("File not Found!");
			e.printStackTrace();
		}
		
	}
	public String getPath() {
		return this.values.get(0);
	}
	private void getPropValues() throws IOException {
		try {
			Properties prop = new Properties();
						
			if(this.is != null) {
				
					prop.load(this.is);
				
			}else {
				throw new FileNotFoundException("Tapir configuration file not found");
			}
			
			String nuXmvPath = prop.getProperty("nuXmvPath");
			this.values.add(nuXmvPath);
		}catch(Exception e){
			System.err.print("Exception" + e);
		}finally {
			this.is.close();
		}
	
	}


}
