package tapir.handlers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

import org.eclipse.jdt.core.dom.*;
import org.eclipse.ui.console.ConsolePlugin;
import org.eclipse.ui.console.IConsole;
import org.eclipse.ui.console.IConsoleManager;
import org.eclipse.ui.console.MessageConsole;
import org.eclipse.ui.console.MessageConsoleStream;

public class Graph {
	public List<Node> nodes = new ArrayList<Node>();
	LinkedList<Integer> edges[];
	int nodeCounter;
	static MessageConsole myConsole = findConsole("TAPIR console");
	static MessageConsoleStream out = myConsole.newMessageStream();

	static private MessageConsole findConsole(String name) {
		ConsolePlugin plugin = ConsolePlugin.getDefault();
		IConsoleManager conMan = plugin.getConsoleManager();
		IConsole[] existing = conMan.getConsoles();
		for (int i = 0; i < existing.length; i++)
			if (name.equals(existing[i].getName()))
				return (MessageConsole) existing[i];
		// no console found, so create a new one
		MessageConsole myConsole = new MessageConsole(name, null);
		conMan.addConsoles(new IConsole[] { myConsole });
		return myConsole;
	}

	public Graph(String source) {
		this.nodeCounter = 20;
		this.initEdges(this.nodeCounter);
		parseMethodSource(source, this);

	}
	public Graph(String source, String methodName) {
		this.nodeCounter = 20;
		this.initEdges(this.nodeCounter);
		parseMethodSource(source, this, methodName);

	}

	public Graph() {
		this.nodeCounter = 100;
		this.initEdges(this.nodeCounter);
	}

	private void initEdges(int size) {
		this.edges = new LinkedList[size];
		for (int i = 0; i < size; i++) {
			this.edges[i] = new LinkedList<>();
		}
	}

	public int countEdge() {
		int count = 0;
		for (int i = 0; i < this.edges.length; i++) {
			if (!this.edges[i].isEmpty()) {
				count++;
			}
		}

		return count;
	}

	private static String parserHelper(String sourc) {
		String[] tempMiddle = sourc.split("\n");
		String sourceStart = "public class Shell {";
		// add a fake class
		String sourceMiddle = "";

		for (String s : tempMiddle) {

			s = s.trim(); // removes extra spaces
			if (s.contains("=") && s.contains(".")) { // check if it's a declaration ; contains equal
				String sDuplicate = s.replaceAll(".+=", "") + " \n";
//				s = s + "\n" + sDuplicate;

			}
			if (s.trim().length() > 0 && !s.startsWith("---") && !s.startsWith("/") && !s.startsWith("*"))
				sourceMiddle += s.trim() + "\n";
		}
		String sourceEnd = "}";
		String source = sourceStart + sourceMiddle + sourceEnd;
		return source;
	}

	public void addEdge(int source, int dest) {
		this.edges[source].add(dest);
	}

	public void parseMethodSource(String src, Graph g) {
		int nodeCount = 0;
//		System.out.println("Code Source = \n" + source);
		ASTParser parser = ASTParser.newParser(AST.JLS11);
		parser.setSource(parserHelper(src).toCharArray());
		parser.setKind(ASTParser.K_COMPILATION_UNIT);
		final CompilationUnit cu = (CompilationUnit) parser.createAST(null);
		g.nodes.add(new Node(0, "EntryPoint", 0, 0));

		ASTVisitor astv = new ASTVisitor() {
			int lineNumber = 0;

			int id = nodeCount;

			public boolean visit(MethodInvocation node) {
				int type = 0;
				id++;

				lineNumber += cu.getLineNumber(node.getStartPosition());
				System.out.println("Method invoked: " + node.getName() + " At line: " + lineNumber + " with ID " + id);
				Node n = new Node(id, node.getExpression() + "#"+node.getName().getFullyQualifiedName(), lineNumber, type);
				g.nodes.add(n);

				g.addEdge(id - 1, id);

				return true;
			}

			public boolean visit(IfStatement node) {

				int type = 1;
				id++;
				int idIf = id;
				lineNumber += cu.getLineNumber(node.getStartPosition());
				Node nif = new Node(id, "IF"+idIf, lineNumber, type);
				g.nodes.add(nif);
				g.addEdge(id - 1, id);
//				System.out.println(" IF Start: " + lineNumber + " with ID " + id);
//				System.out.println("IfStatement -- content:" + node.toString());
				node.getThenStatement().accept(this);
				id++;
				g.addEdge(id - 1, id);
				Node nEndIf = new Node(id, "ENDIF"+idIf, lineNumber, type);
				g.nodes.add(nEndIf);
				g.addEdge(idIf, id);
//				g.addEdge(7, 8);
//				g.addEdge(0, 20);
//				if (node.getElseStatement() != null) {
//					type = 4;
//					id++;
//					Node nElse = new Node(id,"ELSE",lineNumber,type);
//					dico.add(nElse);
//					
//					System.out.println("Else "+ lineNumber+ " with ID " + id);
//					node.getElseStatement().accept(this);
//					nElse.addNext(id);
//					// nelse add to next eleme? later need to add endTypes
//					
//					System.out.println("Else End ");
//
//				}

//				System.out.println(" IF End");
				return false;
			}

//			
			public boolean visit(WhileStatement node) {
				int type = 5;
				id++;
				int idWhile = id;
				lineNumber += cu.getLineNumber(node.getStartPosition());
				Node nWhile = new Node(id, "WHILE", lineNumber, type);
				g.nodes.add(nWhile);
				g.addEdge(id - 1, id);
//				System.out.println(" While Start: " + lineNumber);
				node.getBody().accept(this);
				id++;
//				Uncomment for correct rendenring of the graph 
//				g.addEdge(id - 1, idWhile);
				g.addEdge(id - 1, id);
				Node nEndWhile = new Node(id, "ENDWHILE", lineNumber, type);
				g.nodes.add(nEndWhile);
				g.addEdge(idWhile, id);
//				System.out.println(" While End ");

				return false;
			}

			public boolean visit(ForStatement node) {
				int type = 3;
				id++;
				int idFor = id;
				lineNumber += cu.getLineNumber(node.getStartPosition());
				Node nFor = new Node(id, "FOR", lineNumber, type);
				g.nodes.add(nFor);
				g.addEdge(id - 1, id);
//				System.out.println(" For loop Start: " + lineNumber);
				node.getBody().accept(this);
				id++;
				g.addEdge(id - 1, id);
				Node nEndFor = new Node(id, "ENDFOR", lineNumber, type);
				g.nodes.add(nEndFor);
				g.addEdge(idFor, id);
//				System.out.println(" For Loop End ");
				return false;
			}

			public boolean visit(DoStatement node) {
				int type = 6;
				id++;
				int idDo = id;
				lineNumber += cu.getLineNumber(node.getStartPosition());
				Node nDo = new Node(id, "DO", lineNumber, type);
				g.nodes.add(nDo);
				g.addEdge(id - 1, id);
//				System.out.println(" Do While Start: " + lineNumber);
				node.getBody().accept(this);
				id++;
				g.addEdge(id - 1, id);
				Node nEndDo = new Node(id, "WHILE", lineNumber, 5);
				g.nodes.add(nEndDo);
				g.addEdge(idDo, id);
//				System.out.println(" Do While End ");
				return false;
			}
		};
		cu.accept(astv);
//		int last = g.getLastNode()
		g.nodes.add(new Node(g.getLastNode(), "ExitPoint", 0, 0));
		g.addEdge(g.getLastNode() - 1, g.getLastNode());
//		g.addEdge(g.getLastNode(), g.getLastNode());

		/*
		 * IfStatement ForStatement WhileStatement DoStatement TryStatement
		 * SwitchStatement SynchronizedStatement
		 */

	}
	public void parseMethodSource(String src, Graph g, String methodName) {
		int nodeCount = 0;
//		System.out.println("Code Source = \n" + source);
		ASTParser parser = ASTParser.newParser(AST.JLS11);
		parser.setSource(parserHelper(src).toCharArray());
		parser.setKind(ASTParser.K_COMPILATION_UNIT);
		final CompilationUnit cu = (CompilationUnit) parser.createAST(null);
		g.nodes.add(new Node(0, "EntryPoint", 0, 0));

		ASTVisitor astv = new ASTVisitor() {
			int lineNumber = 0;

			int id = nodeCount;

			public boolean visit(MethodInvocation node) {
				int type = 0;
				id++;

				lineNumber += cu.getLineNumber(node.getStartPosition());
				System.out.println("Method invoked: " + node.getName() + " At line: " + lineNumber + " with ID " + id);
				Node n = new Node(id, node.getExpression() + "#"+node.getName().getFullyQualifiedName(), lineNumber, type);
				g.nodes.add(n);

				g.addEdge(id - 1, id);

				return true;
			}

			public boolean visit(IfStatement node) {

				int type = 1;
				id++;
				int idIf = id;
				lineNumber += cu.getLineNumber(node.getStartPosition());
				Node nif = new Node(id, "IF"+idIf, lineNumber, type);
				g.nodes.add(nif);
				g.addEdge(id - 1, id);
//				System.out.println(" IF Start: " + lineNumber + " with ID " + id);
//				System.out.println("IfStatement -- content:" + node.toString());
				node.getThenStatement().accept(this);
				id++;
				g.addEdge(id - 1, id);
				Node nEndIf = new Node(id, "ENDIF"+idIf, lineNumber, type);
				g.nodes.add(nEndIf);
				g.addEdge(idIf, id);
//				g.addEdge(7, 8);
//				g.addEdge(0, 20);
//				if (node.getElseStatement() != null) {
//					type = 4;
//					id++;
//					Node nElse = new Node(id,"ELSE",lineNumber,type);
//					dico.add(nElse);
//					
//					System.out.println("Else "+ lineNumber+ " with ID " + id);
//					node.getElseStatement().accept(this);
//					nElse.addNext(id);
//					// nelse add to next eleme? later need to add endTypes
//					
//					System.out.println("Else End ");
//
//				}

//				System.out.println(" IF End");
				return false;
			}

//			
			public boolean visit(WhileStatement node) {
				int type = 5;
				id++;
				int idWhile = id;
				lineNumber += cu.getLineNumber(node.getStartPosition());
				Node nWhile = new Node(id, "WHILE", lineNumber, type);
				g.nodes.add(nWhile);
				g.addEdge(id - 1, id);
//				System.out.println(" While Start: " + lineNumber);
				node.getBody().accept(this);
				id++;
//				Uncomment for correct rendenring of the graph 
//				g.addEdge(id - 1, idWhile);
				g.addEdge(id - 1, id);
				Node nEndWhile = new Node(id, "ENDWHILE", lineNumber, type);
				g.nodes.add(nEndWhile);
				g.addEdge(idWhile, id);
//				System.out.println(" While End ");

				return false;
			}

			public boolean visit(ForStatement node) {
				int type = 3;
				id++;
				int idFor = id;
				lineNumber += cu.getLineNumber(node.getStartPosition());
				Node nFor = new Node(id, "FOR", lineNumber, type);
				g.nodes.add(nFor);
				g.addEdge(id - 1, id);
//				System.out.println(" For loop Start: " + lineNumber);
				node.getBody().accept(this);
				id++;
				g.addEdge(id - 1, id);
				Node nEndFor = new Node(id, "ENDFOR", lineNumber, type);
				g.nodes.add(nEndFor);
				g.addEdge(idFor, id);
//				System.out.println(" For Loop End ");
				return false;
			}

			public boolean visit(DoStatement node) {
				int type = 6;
				id++;
				int idDo = id;
				lineNumber += cu.getLineNumber(node.getStartPosition());
				Node nDo = new Node(id, "DO", lineNumber, type);
				g.nodes.add(nDo);
				g.addEdge(id - 1, id);
//				System.out.println(" Do While Start: " + lineNumber);
				node.getBody().accept(this);
				id++;
				g.addEdge(id - 1, id);
				Node nEndDo = new Node(id, "WHILE", lineNumber, 5);
				g.nodes.add(nEndDo);
				g.addEdge(idDo, id);
//				System.out.println(" Do While End ");
				return false;
			}
		};
		cu.accept(astv);
//		int last = g.getLastNode()
		g.nodes.add(new Node(g.getLastNode(), "ExitPoint_"+methodName, 0, 0));
		g.addEdge(g.getLastNode() - 1, g.getLastNode());
//		g.addEdge(g.getLastNode(), g.getLastNode());

		/*
		 * IfStatement ForStatement WhileStatement DoStatement TryStatement
		 * SwitchStatement SynchronizedStatement
		 */

	}

	public int getLastNode() {

		return this.nodes.size() - 1;

	}
//	public void buildNodeDico() {
//		Iterator<String> it = this.list.iterator();
//		int key = 0;
//		// Loop through the list
//		while (it.hasNext()) {
//			/**
//			 * TODO Manipulate the data inside the list Create node object
//			 */
//			Node nd = new Node(key, it.next(), key);
//			this.dico.add(nd);
//			key++;
//		}
//	}

	public String printDico() {
		Iterator<Node> it = this.nodes.iterator();
		String res = "";
		while (it.hasNext()) {
			res += it.next().getLabel() + "\n";
		}

		return res;
	}

	public List<String> getPredicate() {
		Iterator<Node> it = this.nodes.iterator();
		List<String> list = new ArrayList<String>();
		while (it.hasNext()) {
			if (it.next().isPredicate()) {
				list.add(it.next().getLabel());
			}
		}
		return list;

	}

	public void printAllEdge() {
		for (int v = 0; v < this.edges.length; v++) {

			System.out.print(printEdge(v));

		}
	}

	public String printEdge(int id) {
		String res = "";
		for (Integer pCrawl : this.edges[id]) {

			res += stacker(Integer.toString(id), "->", Integer.toString(pCrawl), "\n");
		}
		return res;
	}

	public String printNode(int id) {
		String res = "";

		res = stacker(Integer.toString(id), " [label = ", this.nodes.get(id).label, " ]", "\n");
		return res;
	}

	public String getNode(int id) {
		String res = "";

		res = stacker(this.nodes.get(id).label);
		return res;
	}

	public void printAllNode() {
		for (int v = 0; v < this.nodes.size(); v++) {
			if (printNode(v) != null) {
				System.out.print(printNode(v));
			}
		}
	}

	/**
	 * 
	 * @return a string that contains all the node inside the graph
	 */
	public String getAllNode() {
		String stack = new String();
		for (int v = 0; v < this.nodes.size(); v++) {
			if (printNode(v) != null) {
				stack += getNode(v);
			}
		}
		return stack;
	}
	public ArrayList<String> returnAllNode() {
		ArrayList<String> nn = new ArrayList<String>();
		for (int v = 0; v < this.nodes.size(); v++) {
			if (printNode(v) != null) {
				nn.add(getNode(v));
			}
		}
		return nn;
	}


	/**
	 * 
	 * @param i
	 * @return a string that contains all the node inside the graph without the
	 *         predicates
	 */
	public String getAllNode(int i) {
		String stack = new String();
		for (Node n : this.nodes) {
//			if (n.containsSomething()) {
//				if (!n.isEntry() && !n.isExit())
					stack += n.label + "\n";
//			}

		}

		return stack;
	}

	public String getNodeLabel(int id) {
		if (id > this.nodes.size() - 1) {
			return null;
		}
	
		return this.nodes.get(id).label;
//		for (Node node : this.nodes) {
//			if (node.id == id) {
//				return node.label;
//			}
//		}
//		return null;
	}

	/**
	 * NuSMV file generator Helper
	 * 
	 * @param id
	 * @return
	 */
	public String printNodeSmv(int id) {
		String res = "";

		res = this.nodes.get(id).label;
		return res;
	}

	/**
	 * NuSMV file generator wrapper
	 * 
	 */
	public void printSmvConf() {
		String stack = "{";
		for (int v = 0; v < this.nodes.size(); v++) {
			if (printNodeSmv(v) != null) {
				stack += printNodeSmv(v);
				stack += ",";
			}
		}
		stack = stack.substring(0, stack.length() - 1) + "};";
		out.println("MODULE main");
		out.println("VAR");
		out.println("	method: " + stack);
		out.println("ASSIGN");
		out.println("	init(method):= " + printNodeSmv(0) + ";");
		out.println("next(method) := case \n");
		for (int v = 0; v < this.edges.length; v++) {

			out.print(printEdgeSmv(v,null));

		}
		out.println("\n	TRUE : method; \n esac;");
	}

	/**
	 * 
	 * @return smv graph in a string
	 */
	public ArrayList<String> getSmvConf() {
		ArrayList<String> list = new ArrayList<String>();
		String stack = "{";
		for (int v = 0; v < this.nodes.size(); v++) {
			if (printNodeSmv(v) != null) {
				stack += printNodeSmv(v);
				stack += ",";
			}
		}
		stack = stack.substring(0, stack.length() - 1) + "};";
		list.add("MODULE main");
		list.add("VAR");
		list.add("	method: " + stack);
		list.add("ASSIGN");
		list.add("	init(method):= " + printNodeSmv(0) + ";");
		list.add("next(method) := case \n");
		for (int v = 0; v < this.edges.length; v++) {
			if (!printEdgeSmv(v,null).isBlank()) {
				list.add(printEdgeSmv(v,null));
			}

		}
		list.add("\n esac;");

		return list;
	}

	public String printEdgeSmv(int id, String s) {
		String res = new String();
		boolean flag = false;
		if (getNodeLabel(id) == null) {
			return res;
		}
		
			
		if(getNodeLabel(id) == "EntryPoint") {
			res += "(method = " + s + ") : ";
			res += "{";
			for (Integer pCrawl : this.edges[id]) {

				res += getNodeLabel(pCrawl) + " , ";
			}
			res = res.substring(0, res.length() - 2) + "}; \n";
			return res;
		}
		if(isPredicate(getNodeLabel(id))){
			res += "(method = " + getNodeLabel(id) + ") : ";
			res += "{";
			for (Integer pCrawl : this.edges[id]) {

				res += getNodeLabel(pCrawl) + " , ";
			}
			res = res.substring(0, res.length() - 2) + "}; \n";
			return res;
		}
			
		res += "(method = ExitPoint_" + getNodeLabel(id) + ") : ";
		res += "{";
		for (Integer pCrawl : this.edges[id]) {
			flag = true;
			res += getNodeLabel(pCrawl) + " , ";
		}
		res = res.substring(0, res.length() - 2) + "}; \n";
		if(!flag) {
			res = new String();
			return res;
		}
			
		return res;
	}
	public String printEntryPointEdgeSmv(int id) {
		String res = new String();
		boolean flag = true;
		if (getNodeLabel(id) == null) {
			return res;
		}
		if((getNodeLabel(id).contains("EntryPoint"))) {
			res += "(method = " + getNodeLabel(id) + ") : ";
			res += "{";
			for (Integer pCrawl : this.edges[id]) {

				res += getNodeLabel(pCrawl) + " , ";
			}
			res = res.substring(0, res.length() - 2) + "}; \n";
			return res;
		}
		if(isPredicate(getNodeLabel(id))){
			res += "(method = " + getNodeLabel(id) + ") : ";
			res += "{";
			for (Integer pCrawl : this.edges[id]) {

				res += getNodeLabel(pCrawl) + " , ";
			}
			res = res.substring(0, res.length() - 2) + "}; \n";
			return res;
		}
		
			if(!getNodeLabel(id).contains("ExitPoint")) {
				
				res += "(method = ExitPoint_" + getNodeLabel(id) + ") : ";
				res += "{";
				for (Integer pCrawl : this.edges[id]) {
					
					
					if(getNodeLabel(pCrawl).equals("ExitPoint_"+"Main#main")) {
						res += "ExitPoint ,";
					}else {
						res += getNodeLabel(pCrawl) + " , ";
					}
					
				}
				res = res.substring(0, res.length() - 2) + "}; \n";
//				if(!flag) {
//					System.err.print("135436425");
//					res = new String();
//				}
				
				
					
				return res;
			
			
		}
		
		
			return res;

	}

	private boolean isPredicate(String nodeLabel) {
		String s = nodeLabel.replaceAll("[0-9]","");
		if(s.equals("ENDIF") || (s.equals("IF"))) {
			return true;
		}
		return false;
	}

	/**
	 * Helper for graph printing
	 * 
	 * @param args
	 * @return stack of args on one single line
	 */
	private String stacker(String... args) {
		String stack = "";
		for (String arg : args) {
			stack += arg;
		}
		stack += "\n";
		return stack;

	}

	/**
	 * Helper to print the graph *on Console* in graphViz syntax
	 */
	public void printGraph() {
		
		out.println("======");
		out.println("digraph G {");
		printAllNode();
		printAllEdge();
		out.println("}");
		out.println("----------------------------");

	}
	public void printGraph(String methodName) {
		System.out.println("Method Name: "+ methodName);
		System.out.println("======");
	
//		printAllNode();
		printAllEdgeSmv();
		System.out.println("}");
		System.out.println("----------------------------");

	}
	public String returnAllEdgeSmv() {
		String s = new String();
		for (int v = 0; v < (this.edges.length); v++) {
			if (!printEdgeSmv(v,null).isBlank()) {
				if(!printEdgeSmv(v,null).contains("ExitPoint"))
					s+=(printEdgeSmv(v,null));
				
			}

		}
		return s;
	}
	
	
	public String returnAllEdgeSmv(String name) {
		String s = new String();
		for (int v = 0; v < (this.edges.length); v++) {
			if (!printEdgeSmv(v,name).isBlank()) {
//				if(!printEdgeSmv(v,name).contains("ExitPoint"))
					s+=(printEdgeSmv(v,name));
				
					
			}

		}
		return s;
	}
	private void printAllEdgeSmv() {
		for (int v = 0; v < (this.edges.length); v++) {
			if (!printEdgeSmv(v,null).isBlank()) {
				System.out.println(printEdgeSmv(v,null));
			}

		}
		
	}

	private int getID(String s) {
	    final int prime = 5;
	    int result = 1;
	    result = prime * result + ((s == null) ? 0 : s.hashCode());
	    return result;
	}

	public String returnEntryPointEdge() {
		String s = new String();
		for (int v = 0; v < (this.edges.length); v++) {
			if (!printEntryPointEdgeSmv(v).isBlank()) {
				s+=(printEntryPointEdgeSmv(v));
			}
		}
		return s;
	}

}
