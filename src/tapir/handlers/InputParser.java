package tapir.handlers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class InputParser {
	File specFile;
	List<String> fileLine;
	List<Specification> pattern;

	public InputParser(File file) {
		this.specFile = file;
		this.fileLine = new ArrayList<String>();
		this.pattern = new ArrayList<Specification>();
		this.readFile();
		this.parseLine();
	}

	public void readFile() {
		BufferedReader reader;
		try {
			reader = new BufferedReader(new FileReader(this.specFile));

			String line = reader.readLine();
			while (line != null) {
				this.fileLine.add(line);
//				System.out.println(line);
				line = reader.readLine();
			}
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * build a specification from each line line sample: G(open -> XF close ) ; open
	 * ; close where open and close are the state to be checked comments start with
	 * *
	 */
	private void parseLine() {
		int iter = 0;
		for (String line : this.fileLine) {
			if (line.startsWith("*") | line.isBlank()) {
				continue;
			}
			String[] elems = line.split(";");
			ArrayList<String> varList = new ArrayList<String>(Arrays.asList(elems));
			varList.remove(0);
			/**
			 * elems[0] contains formula elems[0++] contains all variables
			 */
			Specification sp = new Specification(elems[0], varList);
			

			this.pattern.add(sp);
		}
	}

	public static void main(String[] args) {
		File file = new File("D:\\workspace\\tapirecommender\\specificationSample.tapir");
		InputParser ip = new InputParser(file);
		System.out.println(ip.pattern.get(0).printMaster());
		System.out.println(ip.pattern.get(0).getVariable());
	}

}
