package tapir.handlers;


import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import org.json.simple.JSONObject;
import org.json.simple.parser.*;

public class LtlParser {
	JSONObject all;
	int flag;
	List<JSONObject> formulas = new ArrayList<JSONObject>();
	List<String> pattern = new ArrayList<String>();
	public LtlParser(FileReader file, int flag) throws Exception {
		this.flag = 0;
		Object obj = new JSONParser().parse(file);
		this.all = (JSONObject) obj;
		
		int key = 0;
		while (this.all.get((Integer.toString(key))) != null) {
			formulas.add((JSONObject) this.all.get((Integer.toString(key))));
			key++;
		}
	}
	
	public LtlParser(FileReader file) throws IOException  {
		Scanner reader = new Scanner(file);
		this.flag = 1;
		while(reader.hasNext()) {
			String s = reader.nextLine();
			if(!s.startsWith("--"))
				this.pattern.add(s);
		}
		reader.close();
	}

	public String getContent() {
		String result = new String();
		for (JSONObject objo : this.formulas) {
			result += objo.toJSONString() + "\n";

		}
		return result;
	}

	private String getRaw(int key) {
		return (String) this.formulas.get(key).get("raw");
	}

	private String getVarList(int key) {

		return this.formulas.get(key).get("varLists").toString();
	}

	private String cleanList(String s) {
		String res = s.replace("[", "");
		res = res.replace("]", "");
		res = res.replace("\"", "");
		return res;
	}

	private String getRaw() {
		String result = new String();
		for (JSONObject objo : this.formulas) {
			result += objo.get("raw") + "\n";

		}
		return result;
	}

	private String getVar() {
		String result = new String();
		for (JSONObject objo : this.formulas) {
			result += objo.get("varLists") + "\n";

		}
		return result;
	}

	private ArrayList<String> buildSpec(int key) {
		ArrayList<String> result = new ArrayList<String>();
		String raw = this.getRaw(key);
//		ArrayList<String> lists = this.getVarList(key);
		String temp = cleanList(this.getVarList(key));
		String[] varList = temp.split(",");
		// get every var inside var List
		for (String s : varList) {
			s = s.replace("{", "");
			s = s.replace("}", "");
			s = s.substring(s.indexOf(":")+1);
			String[] ss = s.split(" ");
			String mid = raw;
			for (String st : ss) {
				mid = mid.replaceFirst(":var", "(method = " + st + " )");

			}
			result.add(mid);
		}
//		raw= raw.replace(":var", varList[0]);
//		result.add(raw);
		return result;
	}
	
	private String nuXmvHelper(String s) {
		return "LTLSPEC " + s + "\n";
	}
	public String printSpec(int key) {
		String result = new String();
		for(String content : this.buildSpec(key) ) {
			result += nuXmvHelper(content);
		}
		return result;
	}
	public String getSpec() {
		String result = "";
		
		Iterator<JSONObject> it = this.formulas.iterator();
		int i = 0;
		while(it.hasNext()) {
			it.next();
			result += this.printSpec(i);
			i++;
		}
		return result;
	}
	public ArrayList<String> getSpecList() {
		ArrayList<String> result = new ArrayList<String>();
		if(flag == 1) {
			Iterator<String> iter = this.pattern.iterator();
			while (iter.hasNext()) {
				String s = iter.next();
				if(!s.startsWith("--"))
					result.add("LTLSPEC " +s);
			}
			return result;
		}
		Iterator<JSONObject> it = this.formulas.iterator();
		
		int i = 0;
		while(it.hasNext()) {
			it.next();
			result.add(this.printSpec(i));
			i++;
		}
		return result;
		
	}
	public String getSpec(int id) {
		
		return this.printSpec(id);
	}
//	public static void main(String[] args) {
//		try {
////			E:\Eclipse-workspace\TapirPluginDev\Specification
//			FileReader f = new FileReader("E:\\Eclipse-workspace\\TapirPluginDev\\Specification\\LtlSpec-2.ltl");
//			LtlParser spec = new LtlParser(f);
////			String raw = spec.getRaw();
////			String var = spec.getVar();
////			System.out.println(raw);
////			System.out.println(var);
//			ArrayList<String> result = spec.getSpecList();
//			System.out.print(result.get(0));
//
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}
}
