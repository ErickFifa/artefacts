package tapir.handlers;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.Assignment;
import org.eclipse.jdt.core.dom.ClassInstanceCreation;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.ConditionalExpression;
import org.eclipse.jdt.core.dom.ConstructorInvocation;
import org.eclipse.jdt.core.dom.DoStatement;
import org.eclipse.jdt.core.dom.ExpressionMethodReference;
import org.eclipse.jdt.core.dom.ExpressionStatement;
import org.eclipse.jdt.core.dom.FieldAccess;
import org.eclipse.jdt.core.dom.ForStatement;
import org.eclipse.jdt.core.dom.IfStatement;
import org.eclipse.jdt.core.dom.InfixExpression;
import org.eclipse.jdt.core.dom.InstanceofExpression;
import org.eclipse.jdt.core.dom.LambdaExpression;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.MethodReference;
import org.eclipse.jdt.core.dom.Name;
import org.eclipse.jdt.core.dom.SimpleType;
import org.eclipse.jdt.core.dom.TryStatement;
import org.eclipse.jdt.core.dom.TypeDeclaration;
import org.eclipse.jdt.core.dom.VariableDeclarationExpression;
import org.eclipse.jdt.core.dom.VariableDeclarationStatement;
import org.eclipse.jdt.core.dom.WhileStatement;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.io.DOTExporter;
import org.jgrapht.io.IntegerComponentNameProvider;
import org.jgrapht.io.StringComponentNameProvider;



@SuppressWarnings("deprecation")
public class Method  {
	private String name;
	private String source;
	private ArrayList<String> methodInvocation;
	 ArrayList<String> predicates;
	Map<String,String> classInstanceList ;
	public DefaultDirectedGraph<String, Scope> directedGraph =  new DefaultDirectedGraph<>(Scope.class) ;
	/**
	 * Constructor
	 * @param name
	 */
	public Method(String name, String source) {
		this.name = name;
		this.source = source;
		this.classInstanceList = new HashMap<String,String>();
		this.predicates = new ArrayList<String>();
		this.methodInvocation = this.fetchMethodInvocation();
		
		this.buildGraph();
	}
	
	public boolean containsInvoke(String s) {
		s = s.strip();
		for (String ss : this.methodInvocation) {
			if(ss.equals(s)) {
				return true;
			}
		}
		return false;
	}
	private ArrayList<String> fetchMethodInvocation() {
		Map<String,String> classInstanceList = this.classInstanceList;
		ArrayList<String> mList = new ArrayList<String>();
		ASTParser parser = ASTParser.newParser(AST.JLS11);
		parser.setSource(parserHelper(this.source).toCharArray());
		parser.setKind(ASTParser.K_COMPILATION_UNIT);
		final CompilationUnit cu = (CompilationUnit) parser.createAST(null);
		ASTVisitor astv = new ASTVisitor() {
			
			public boolean visit(ClassInstanceCreation node) {
				
				classInstanceList.put(clean(inferType(node.getParent().toString())), clean(node.getType().toString()));
				//parse the parent type, 
			
				return false;
			}
			
			public boolean visit(VariableDeclarationStatement node) {
//				System.out.println("Vds = "+ node.toString());
				String[] key = node.fragments().get(0).toString().strip().split("="); //split at = and get first
				
				classInstanceList.put(key[0], clean(node.getType().toString()));

				return false;
			}
			public boolean visit(MethodInvocation node) {
//				List<node> args = node.arguments();
				if(!Objects.equals(null, node.getExpression())) {
					String temp ;
					if(node.getExpression().toString().contains(".")) {
					
						String[] type = node.getExpression().toString().strip().replace(".", "#").split("#");
						temp = type[0];
						node.getExpression().accept(this);
					}else {
						temp = node.getExpression().toString();
					}
					
					
					
					mList.add(typeConverter(temp) +"#" + node.getName().toString());
				}
				
				return false;
				
//				
//				if(!Objects.equals(null, node.getExpression())) {
//					if(node.getExpression().toString().contains(".")) {
//					
//						String[] type = node.getExpression().toString().strip().replace(".", "#").split("#");
//						temp = type[0];
//						node.getExpression().accept(this);
//					}else {
//						temp = node.getExpression().toString();
//					}
//					
//					String d = typeConverter(temp) +"#" + node.getName().toString();
//					graph.addVertex(d);
//					graph.addEdge(prevVertex, d, new Scope(entryPointName));
//					prevVertex = d;
//					lastNode.add(prevVertex);
			}
			
			
//			public boolean visit(Assignment node) {
//				System.out.println("left = "+ node.getLeftHandSide());
//				System.out.println("right = "+ node.getRightHandSide());
//				return true;
//			}
//			
//			public boolean visit(ExpressionMethodReference node) {
//				System.out.println("ExpressionMethodReference = "+ node.getExpression());
//				return false;
//			}
////			
//			public boolean visit(FieldAccess node) {
//				System.out.println("FieldAccess = "+ node.getExpression());
//				return false;
//			}
//			
//			public boolean visit(InfixExpression node) {
//				System.out.println("InfixExpression L = "+ node.getLeftOperand());
//				System.out.println("InfixExpression R = "+ node.getRightOperand());
//				return false;
//			}
			
//			public boolean visit(InstanceofExpression node) {
//				System.out.println("InstanceofExpression = "+ node.getLeftOperand());
//				return false;
//			}
//			
//			public boolean visit(LambdaExpression node) {
//				System.out.println("LambdaExpression = "+ node.getBody().toString());
//				return false;
//			}
//			
//			public boolean visit(MethodReference node) {
//				System.out.println("MethodReference = "+ node.getTypeArgumentsProperty().toString());
//				return false;
//			}
//			public boolean visit(Name node) {
//				System.out.println("Name = "+ node.getFullyQualifiedName());
//				return false;
//			}
		};
		
		cu.accept(astv);
		return mList;
		
		
		
	}
	
	
	private  String typeConverter(String nodeExpression) {
		//remove all args in the constructor if there is.
			String temp = clean(nodeExpression);
			if(this.classInstanceList.getOrDefault(temp, temp) != "") {
				return this.classInstanceList.getOrDefault(temp, temp);
			}else {
				return this.name;
			}
			
	
	}
	
	private static String clean(String s) {
		String temp = s.replaceAll("\\(.*\\)", "");
		temp = temp.replaceAll("<(.*?)>", "");
		temp = temp.replace("new", "");
		temp = temp.replace("<", "");
		temp = temp.replace(">", "");
		temp = temp.replace("(", "");
		temp = temp.replace(")", "");
		temp = temp.replaceAll("\\[.*\\]", "");
		temp = temp.strip();
		if(temp.startsWith("#"))
			temp = temp.replaceFirst("#", "");
		return temp;
	}
	
	
	private String inferType(String s) {
		String temp[] = s.split("=");
		
		return temp[0];
	}
	
	public String getName() {
		return this.name;
	}
	
	public String getSource() {
		return source;
	}
	
	public ArrayList<String> getMethodInvocationList(){
		return this.methodInvocation;
	}
	
	protected boolean containsInvocation() {
		return !this.methodInvocation.isEmpty();
	}
	
	/**
	 * Format the source to be accepted as a valid format by the ASTParser
	 * @param sourc
	 * @return
	 */
	private static String parserHelper(String sourc) {
		String[] tempMiddle = sourc.split("\n");
		String sourceStart = "public class Shell {";
		// add a fake class
		String sourceMiddle = "";

		for (String s : tempMiddle) {

			s = s.trim(); // removes extra spaces
			if (s.contains("=") && s.contains(".")) { // check if it's a declaration ; contains equal
//				s = s + "\n" + sDuplicate;

			}
			if (s.trim().length() > 0 && !s.startsWith("---") && !s.startsWith("/") && !s.startsWith("*"))
				sourceMiddle += s.trim() + "\n";
		}
		String sourceEnd = "}";
		String source = sourceStart + sourceMiddle + sourceEnd;
		return source;
	}
	
	private void buildGraph(){
		DefaultDirectedGraph<String, Scope> graph =  this.directedGraph;
		String entryPointName  =  this.getName();
		String exitPoint = "Exit"+this.getName();
		ArrayList<String> lastNode = new ArrayList<String>(); 
		ArrayList<String> predica = this.predicates;
		graph.addVertex(entryPointName);
		lastNode.add(entryPointName);
		graph.addVertex(exitPoint);
		
		ASTParser parser = ASTParser.newParser(AST.JLS11);
		parser.setSource(parserHelper(this.source).toCharArray());
		parser.setKind(ASTParser.K_COMPILATION_UNIT);
		final CompilationUnit cu = (CompilationUnit) parser.createAST(null);
		ASTVisitor astv = new ASTVisitor() {
			int  id =0;
			String prevVertex = entryPointName;
			
			
//			public boolean visit(ConstructorInvocation node) {
//				System.out.println("ConstructorInvocation = "+ node.toString());
//				return false;
//			}
//			public boolean visit(ExpressionStatement node) {
//				System.out.println("ExpressionStatement = "+ node.toString());
//				return false;
//			}
			
			
			
//			public boolean visit(MethodInvocation node) {
//				//add a vertex here
//				if(!Objects.equals(null, node.getExpression())) {
//					System.out.println("GetExpression = "+ node.getExpression());
////					String temp = node.getName().toString().replace(".", "#");
//					String temp = node.getExpression().toString().replace(".", "#");
//					String d = typeConverter(temp) +"#" + node.getName().toString();
//					graph.addVertex(d);
//					graph.addEdge(prevVertex, d, new Scope(entryPointName));
//					prevVertex = d;
//					lastNode.add(prevVertex);
//					
//				}
//				
//				return false;
//			}
			
			
			
			public boolean visit(MethodInvocation node) {
				//add a vertex here
				
//				if(!Objects.equals(null, node.getExpression())) {
////					node.getExpression().accept(this);
//				
//				}
				
//					String temp = node.getName().toString().replace(".", "#");
//					String temp = node.getExpression().toString().replace(".", "#");
					
					String temp ;
					if(!Objects.equals(null, node.getExpression())) {
					if(node.getExpression().toString().contains(".")) {
					
						String[] type = node.getExpression().toString().strip().replace(".", "#").split("#");
						temp = type[0];
						node.getExpression().accept(this);
					}else {
						temp = node.getExpression().toString();
					}
					
					String d = typeConverter(temp) +"#" + node.getName().toString();
					
					graph.addVertex(d);
					graph.addEdge(prevVertex, d, new Scope(entryPointName));
					prevVertex = d;
					lastNode.add(prevVertex);
					}
					
				
				
				return false;
			}
			
			public boolean visit(IfStatement node) {
				
					id++;
					String d = "IF" + id+entryPointName;
					String idIF = d;
					graph.addVertex(d);
					predica.add(d);
					graph.addEdge(prevVertex, d , new Scope(entryPointName));
					prevVertex = d;
					node.getExpression().accept(this);
					String condExpression = prevVertex;
					//TODO replaced prevVertex place
					node.getThenStatement().accept(this);
					graph.addVertex("END"+idIF);
					graph.addEdge(prevVertex, "END"+idIF , new Scope(entryPointName));
					if(node.getElseStatement() != null) {
						prevVertex = idIF;
//						
						node.getElseStatement().accept(this);
						graph.addEdge(prevVertex, "END"+idIF , new Scope(entryPointName));
					}else {
						graph.addEdge(condExpression,"END"+idIF , new Scope(entryPointName));
					}
					
					prevVertex = ("END"+idIF);
					predica.add(prevVertex);
					lastNode.add(prevVertex);
				
//				else {
//					id++;
//					String d = "IF" + id+entryPointName;
//					String idIF = d;
//					graph.addVertex(d);
//					predica.add(d);
//					graph.addEdge(prevVertex, d , new Scope(entryPointName));
//					prevVertex = d;
//					node.getThenStatement().accept(this);
//					graph.addVertex("END"+idIF);
//					graph.addEdge(prevVertex, "END"+idIF , new Scope(entryPointName));
//					node.getElseStatement().accept(this);
//					graph.addEdge(prevVertex, "END"+idIF , new Scope(entryPointName));
//					prevVertex = ("END"+idIF);
//					predica.add(prevVertex);
//					lastNode.add(prevVertex);
//				}
				
				return false;
			}
			
			public boolean visit(WhileStatement node) {
				id++;
				String d ="While" +id+entryPointName;
				String idWhile = d;
				graph.addVertex(d);
				predica.add(d);
				graph.addEdge(prevVertex, d , new Scope(entryPointName));
				prevVertex = d;
				node.getBody().accept(this);
				node.getExpression().accept(this);
				graph.addVertex("END"+idWhile);
				graph.addEdge(prevVertex, "END"+idWhile , new Scope(entryPointName));
				graph.addEdge("END"+idWhile, idWhile, new Scope(entryPointName));
				predica.add("END"+idWhile);
				prevVertex = idWhile;
				lastNode.add(prevVertex);
				return false;
			}
			
			public boolean visit(ForStatement node) {
				if(node.getExpression() != null)
					node.getExpression().accept(this);
				id++;
				String d ="For" +id+entryPointName;
				String idFor = d;
				graph.addVertex(d);
				predica.add(d);
				graph.addEdge(prevVertex, d , new Scope(entryPointName));
				prevVertex = d;
				node.getBody().accept(this);
				graph.addVertex("END"+idFor);
				graph.addEdge(prevVertex, "END"+idFor , new Scope(entryPointName));
				graph.addEdge("END"+idFor, idFor, new Scope(entryPointName));
				prevVertex = idFor;
				predica.add(prevVertex);
				lastNode.add(prevVertex);
				return false;
			}
			
			public boolean visit(DoStatement node) {
				id++;
				String d ="Do" +id+entryPointName;
				String idDo = d;
				graph.addVertex(d);
				predica.add(d);
				graph.addEdge(prevVertex, d , new Scope(entryPointName));
				prevVertex = d;
				node.getBody().accept(this);
				node.getExpression().accept(this);
				graph.addVertex("WhileDO"+idDo);
				graph.addEdge(prevVertex, "WhileDO"+idDo , new Scope(entryPointName));
				graph.addEdge("WhileDO"+idDo, idDo, new Scope(entryPointName));
				prevVertex = "WhileDO"+idDo;
				predica.add(prevVertex);
				lastNode.add(prevVertex);
				
				return false;
			}
			
			public boolean visit(TryStatement node) {
				id++;
				String d ="Try" +id+entryPointName;
				String idTry = d;
				graph.addVertex(d);
				predica.add(d);
				graph.addEdge(prevVertex, d , new Scope(entryPointName));
				prevVertex = d;
				node.getBody().accept(this);
				
				graph.addVertex("END"+idTry);
				graph.addEdge(prevVertex, "END"+idTry , new Scope(entryPointName));
				lastNode.add(prevVertex);
				prevVertex = "END"+idTry;
				if(node.getFinally() != null) {
					node.getFinally().accept(this);		
				}
				predica.add(prevVertex);
				lastNode.add(prevVertex);
				return false;
			}
			
			
			public boolean visit(ConditionalExpression node) {

				id++;
				String d = "Cond" + id+entryPointName;
				String idCond = d;
				graph.addVertex(d);
				predica.add(d);
				graph.addEdge(prevVertex, d , new Scope(entryPointName));
				prevVertex = d;
				node.getExpression().accept(this);
				node.getThenExpression().accept(this);
				graph.addVertex("END"+idCond);
				graph.addEdge(prevVertex, "END"+idCond , new Scope(entryPointName));
				if(node.getElseExpression() != null) {
					prevVertex = idCond;
//					
					node.getElseExpression().accept(this);
					graph.addEdge(prevVertex, "END"+idCond , new Scope(entryPointName));
				}else {
					graph.addEdge(idCond,"END"+idCond , new Scope(entryPointName));
				}
				
				prevVertex = ("END"+idCond);
				predica.add(prevVertex);
				lastNode.add(prevVertex);
				
				return false;
			}
			
		};
		cu.accept(astv);
		graph.addEdge(lastNode.get(lastNode.size()-1) , exitPoint,new Scope(entryPointName));
	}
	
	

	/**
	 * Export the graph in dot syntax with the following file format: 
	 * tempFolder//ClassName#MethodName.dot
	 */
	public void exportGraph() {
		FileWriter writer;
		try {
			writer = new FileWriter(Config.TEMPFOLDER()+"\\"+this.getName()+".dot");
			BufferedWriter bwr = new BufferedWriter(writer);
			
			DOTExporter<String, Scope> exporter =
		            new DOTExporter<String, Scope>( new IntegerComponentNameProvider<String>(), // vertex ids
		            		   new StringComponentNameProvider<String>(),  // vertex labels
		            		   null, //edge labels
		            		  null, // vertex attributes
		            		   null); // edge attributes);
			exporter.exportGraph(this.directedGraph, bwr);
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		 
	}
	
	protected String getSmvModel() {
		String result = "";
		for(String s :this.directedGraph.vertexSet()) {
			for(DefaultEdge ss : this.directedGraph.outgoingEdgesOf(s) ) {
				result +=  ss + "\n";
			}
			
		}
						
		return result;
	}
	
	
	public boolean containsAPICall() {
		for(String s : this.methodInvocation) {
			if(Config.APIMETHOD.contains(s))
				return true;
		}
		
		return false;
	}


}
