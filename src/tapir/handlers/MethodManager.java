package tapir.handlers;

import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.JavaModelException;

public class MethodManager {
	IMethod method;

	public MethodManager(IMethod m) {
		this.method = m;
	}

	public String getSource() throws JavaModelException {
		return this.method.getSource();
	}
	
	public String getName() {
	
//		return this.method.getElementName()+"#" +this.method.getParent().getElementName();
		return this.method.getParent().getElementName() + "#" + this.method.getElementName() ;
		
//		return this.method.getElementName()+ "#" +this.method.getParent().getElementName();
	}
}
