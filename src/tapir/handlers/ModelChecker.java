/**
 * 
 */
package tapir.handlers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import tapir.views.Configuration;

/**
 * @author erick
 *
 */
public class ModelChecker {
	String modelChecker;
	List<String> args;
	List<String> output;

	public ModelChecker() {

		this.modelChecker = readPropHelper();
		this.args = new ArrayList<String>();
		this.output = new ArrayList<String>();
//		String temporaryFolder = Configuration.tapirConfiguration[0];
		String temporaryFolder = Config.TEMPFOLDER();
		// if on windows
		if (System.getProperty("os.name").toLowerCase().contains("win")) {
			this.args.add("cmd.exe");
			this.args.add("/c");
		} 

//		this.args.add("dir");
		this.args.add("nuxmv");
//		Add a relative path to nuxmv for testing purpose
//		this.args.add("C:\\Users\\raelijoe\\Downloads\\nuXmv-2.0.0-win64\\bin\\nuXmv.exe");
		this.args.add(temporaryFolder + "\\graph.smv");
//		this.args.add("test.smv");
	}

	private static String readPropHelper() {
//		GetPropertyValues pp = new GetPropertyValues("tapir.properties");
		return new GetPropertyValues("tapir.properties").getPath();
	}

	public void addArgs(String... args) {
		for (String arg : args) {
			this.args.add(arg);
		}
	}

	public void cleanArgs() {
		this.args.clear();
	}

	public void runChecker() throws IOException, InterruptedException {

		// Build args into a single str
//		for (String arg : this.args) {
//			command += arg;
//		}
		ProcessBuilder build = new ProcessBuilder();
		build.command(this.args);
		build.directory();
//		build.command("-h");
		Process proc = build.start();
		int retVal = proc.waitFor();
		if (retVal != 0) {
			System.err.print("Error");
		} else {
			this.getOutput(proc);
			System.out.println("Output from check: " + this.output);
		}

	}

	private void getOutput(Process p) throws IOException {
		String line;
		BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
	
		while ((line = input.readLine()) != null) {
			if(line.startsWith("--"))
				this.output.add(line);
		}
		input.close();

	}

	private void getErrOutput(Process p) throws IOException {
		String line;
		BufferedReader input = new BufferedReader(new InputStreamReader(p.getErrorStream()));
		while ((line = input.readLine()) != null) {
			if(line.startsWith("--"))
			this.output.add(line);
		}
		input.close();

	}

	public String printOutput() {
		String results = new String();
		for (String result : this.output) {
			results += result;
		}
		return results;
	}

	private void outputSimulator(String fileName) {
		try {
			File file = new File(System.getProperty("user.dir") + "\\resources\\" + fileName);
			Scanner scanner = new Scanner(file);
			scanner.useDelimiter("\r\n");
			while (scanner.hasNext()) {
				this.output.add(scanner.next().trim());
			}
			scanner.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

	}

	/**
	 * remove extra reply from NuXmv
	 */
	private void removeExtra() {

		// check if output is set
		if (this.output.isEmpty()) {
			return;
		}
		Iterator it = this.output.iterator();
		while (it.hasNext()) {
			if (it.next().toString().contains("***")) {
				it.remove();
			}
		}

	}

	/**
	 * 
	 * @return false if one of the specification is false
	 */
	public String getAnswer() {
		
		Iterator<String> it = this.output.iterator();
		//remove useless line (Starts with (***))
		while (it.hasNext()) {
			String answer = it.next().toString();
			
			if (answer.contains("false")) {
				this.output.clear();
				return "Possible Violation Found";
			}
			if (answer.contains("undefined")) {
				this.output.clear();
				return "Doesn't apply";
			}
			
			if(answer.contains("syntax error")) {
				this.output.clear();
				return "Syntax Error";
			}
				

		}
		this.output.clear();
		return "True";
	}

	public void initModel() {

		try {
			ProcessBuilder builder = new ProcessBuilder();
			boolean isWindows = true;

			builder.command(this.args);
			builder.directory();
			Process process = builder.start();
			this.getOutput(process);
			this.getErrOutput(process);
			int val = process.waitFor();
			if (val== 0) {
				System.out.println("--exit =0");
//				this.getOutput(process);
			} else {
				System.out.println("--exit code:" + val );
//				this.getErrOutput(process);
				
			}
//			this.removeExtra();

		} catch (Exception e) {
			System.err.println("Something went wrong: " + e);
		}

	}

	protected String checkSpec(String spec) {
		String result = new String();
		try {
			// Append spec on file
			String path = Config.TEMPFOLDER() + "\\graph.smv";
			File file = new File(path);
			FileWriter fr = new FileWriter(file, true);
			fr.write(spec);
			fr.close();
			this.output.clear(); //clear the output container of previous answer
			this.initModel();
			result = this.getAnswer();
			deleteLastline(path);
			return result;

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;

	}
	
	protected ArrayList<String> checkSpec(String spec,boolean b){
		ArrayList<String> result = new ArrayList<String>();
		ArrayList<String> temp = new ArrayList<String>();
		try {
			// Append spec on file
			String path = Config.TEMPFOLDER() + "\\graph.smv";
			File file = new File(path);
			FileWriter fr = new FileWriter(file, true);
			fr.write(spec);
			fr.close();
			this.output.clear(); //clear the output container of previous answer
			this.initModel();
			result.addAll(this.output);
			deleteLastline(path);
			for(String s : result) {
				s = s.strip();
				if(s.startsWith("***"))
					continue;
				if(s.startsWith("state")) {
					String[] tempS = s.split("=");
					
					temp.add(tempS[1].strip());
				}
					
			}
			
			return temp;

		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;
		
		
	}
	/**
	 * Delete the last line (LTLSPEC) of a smv file after the model checking is done.
	 * this method is used by {@link #checkSpec(String)}
	 * @param path
	 */
	private static void deleteLastline(String path) {
		try {
			RandomAccessFile f = new RandomAccessFile(path, "rw");
			long length;
			length = f.length() - 1;
			byte b;
			do {
				length -= 1;
				f.seek(length);
				b = f.readByte();
			} while (b != 10);
			f.setLength(length + 1);
			f.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

//	public static void main(String[] args) {
//		ModelChecker m = new ModelChecker();
//		
//		m.initModel();
//		
//		System.out.print(m.checkSpec("asf"));
//	}

}
