package tapir.handlers;

import java.util.ArrayList;
import java.util.List;

public class Node implements Comparable<Node> {
	int id;
	int position;
	List<Integer> next = new ArrayList<Integer>();
	List<Integer> previous = new ArrayList<Integer>();
	String label;
	NodeType type;
	boolean visited;

	public Node(int id, String s, int pos, int type) {
		this.id = id;
		this.label = s;
		this.position = pos;
		this.type = getNodeType(type);
		this.visited = false;
	}
	public Node(int id,String label) {
		this.id = id;
		this.label = label;
	}

	public String getLabel() {
		return this.label;
	}

	public boolean containsSomething() {
		return this.label.isEmpty();
	}

	public int getPosition() {
		return this.position;
	}

	public int getId() {
		return this.id;
	}

	public void visit() {
		this.visited = true;
	}

	public boolean visited() {
		return (this.visited) ? true : false;
	}

	public boolean isPredicate() {
		if (this.type == NodeType.CALL) {
			return false;
		}
		return true;
	}
	public boolean isEntry() {
		if(this.label == "EntryPoint") {
			return true;
		}
		return false;
	}
	public boolean isExit() {
		if(this.label == "ExitPoint") {
			return true;
		}
		return false;
	}
	public void addNext(int n) {
		this.next.add(n);
	}

	public void removeNext(Node n) {
		if (this.next.contains(n)) {
			this.next.remove(this.next.indexOf(n));
		} else {
			System.out.println("Node doesn't exist");
		}
	}

	public void removeNext() {
		this.next.clear();
	}

	public void addPrevious(int n) {
		this.previous.add(n);
	}

	public void removePrevious(Node n) {
		if (this.previous.contains(n)) {
			this.previous.remove(this.previous.indexOf(n));
		} else {
			System.out.println("Node doesn't exist");
		}
	}

	public void removePrevious() {
		this.previous.clear();
	}

	@Override
	public int compareTo(Node other) {
		if (other.getPosition() > this.getPosition()) {
			return 1;
		}
		return 0;
	}

	private NodeType getNodeType(int s) {
		switch (s) {
		case 1:
			return NodeType.IF;
		case 2:
			return NodeType.ELSEIF;
		case 3:
			return NodeType.FOR;

		case 4:
			return NodeType.ELSE;

		case 5:
			return NodeType.WHILE;

		case 6:
			return NodeType.DO;
		case 7:
			return NodeType.SWITCH;
		case 8:
			return NodeType.CASE;
		case 9:
			return NodeType.BREAK;
		case 10:
			return NodeType.RETURN;
		case 11:
			return NodeType.CONTINUE;
		case 12:
			return NodeType.TRY;
		case 13:
			return NodeType.CATCH;
		case 0:
			return NodeType.CALL;
		default:
			return NodeType.UNKWOWN;
		}
	}

}
