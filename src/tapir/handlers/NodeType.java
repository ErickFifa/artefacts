package tapir.handlers;

public enum NodeType {
	CALL,
	WHILE,
	FOR,
	DO,
	IF,
	ELSE,
	ELSEIF,
	CASE,
	SWITCH,
	BREAK,
	RETURN,
	CONTINUE,
	TRY,
	CATCH,
	UNKWOWN;
}
