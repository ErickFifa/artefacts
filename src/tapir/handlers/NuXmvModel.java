package tapir.handlers;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;

public class NuXmvModel {
	List<Classe> classList;
	String entryPointClass;
	String entryPointMethod;
	DefaultDirectedGraph<String, Scope> graph;
	Map<String, ArrayList<String>> scope;

	/**
	 * class constructor
	 * 
	 * @param className
	 * @param method
	 */
	protected NuXmvModel(List<Classe> classeList, String classe, String method,
			DefaultDirectedGraph<String, Scope> graph) {
		this.entryPointClass = classe;
		this.entryPointMethod = method;
		this.classList = classeList;
		this.graph = graph;
		this.scope = new HashMap<String, ArrayList<String>>();
	}

	/**
	 * class constructor and define entryPoint at method main() in class Main
	 * 
	 * @param classeList
	 * @param methodList
	 */
	protected NuXmvModel(List<Classe> classeList, DefaultDirectedGraph<String, Scope> graph) {
		this.entryPointClass = "Main";
		this.entryPointMethod = "main";
		this.classList = classeList;
		this.graph = graph;
		this.scope = new HashMap<String, ArrayList<String>>();
	}

	/**
	 * 
	 * @return a list of all state ( method invocation ) of the project and create a
	 *         scope in the state machine using boolean on non-empty method
	 *         invocation
	 */
	private String getVarList() {
		ArrayList<String> temp = new ArrayList<String>();

		/**
		 * Fetch and format all the method invocation inside the project
		 */
		for (Classe c : this.classList) {
			for (Method cm : c.methods) {
				Set<String> sms = cm.directedGraph.vertexSet();
				for(String sm : sms) {
					if(!temp.contains(sm))
						temp.add(sm);
				}
				// for all method invocation

			}
		}
		String result = " VAR state : { ";
		for (String s : temp) {
			result += s + " , ";
		}
		result = result.substring(0, result.length() - 2);
		result += " }; \n";

		temp = new ArrayList<String>();
		for (Classe c : this.classList) {
			for (Method cm : c.methods) {
//				if (cm.containsInvocation())
//				if(!this.isEntryPoint(cm.getName())) {
				if (!temp.contains(cm.getName()))
					temp.add(cm.getName());
//				}

			}
		}
		for (String s : temp) {
			result += "e" + s + " : boolean; \n";
		}

		return result;
	}

	/**
	 * 
	 * @return all the initialized variable in SMV syntax
	 */
	private String getAssignInit() {
		String result = "ASSIGN \n";
		ArrayList<String> temp = new ArrayList<String>();
		/**
		 * initialize all variable;
		 */
		for (Classe c : this.classList) {
			for (Method cm : c.methods) {
//				if(!this.isEntryPoint(cm.getName()))
				temp.add(cm.getName());
			}
		}
		for (String s : temp) {
			if (s.equals(this.entryPointClass + "#" + this.entryPointMethod)) {
				if(!result.contains("init(e" + s + ") := TRUE; \n"))
					result += "init(e" + s + ") := TRUE; \n";
			} else {
				if(!result.contains("init(e" + s + ") := FALSE; \n"))
				result += "init(e" + s + ") := FALSE; \n";
			}

		}

		result += "init(state) := " + this.entryPointClass + "#" + this.entryPointMethod + "; \n";

		return result;
	}

	private boolean isEntryPoint(String m) {
		String s[] = m.split("#");
		if ((s[0].equals(this.entryPointClass))) {
			if ((s[1].equals(this.entryPointMethod))) {
				return true;
			}

		}
		return false;
	}

	/**
	 * 
	 * @return all the boolean value to bind the edge of each state to a scope of
	 *         the method containing the call
	 */
//	private String getNextStateScope() {
//		String result = "";
//		// Get all method declaration
//		for (Classe c : this.classList) {
//			for (Method cm : c.methods) {
////				if(!this.isEntryPoint(cm.getName())) {
//				String s = cm.getName();
//				result += "next(e" + s + ") := case \n";
//				result += this.addStateScope(s, "TRUE");
//				for (String invok : cm.getMethodInvocationList()) {
//					result += this.addStateScope(invok, "TRUE");
//				}
//				for (String pre : cm.predicates) {
//					result += this.addStateScope(pre, "TRUE");
//				}
//
//				result += "TRUE : FALSE; \n";
//				result += "esac; \n \n";
//			}
//
////			}
//		}
//
//		return result;
//	}

	private String getNextStateScope() {
		String result = new String();
		Iterator<String> it = this.scope.keySet().iterator();
		while (it.hasNext()) {
			String key = it.next();
			result += "next(e" + key + ") := case \n";
			for (String s : this.scope.get(key)) {

				result += this.addStateScope(s);
			}
			result += "TRUE : FALSE; \n";
			result += "esac; \n";
		}

		return result;
	}

//	private String getNextState() {
//		String result = new String();
//		// method call should be organized as a graph
////		result += this.addState(this.entryPointClass + "#" + this.entryPointMethod, dest);
//		result += "next(state) := case \n";
//		for (Classe c : this.classList) {
//			for (Method cm : c.methods) {
//
//				if (!this.isEntryPoint(cm.getName())) {
//					for (String s : cm.directedGraph.vertexSet()) {
//						String scope = "e" + cm.getName();
//						for (DefaultEdge ss : cm.directedGraph.outgoingEdgesOf(s)) {
//							// if source is predicates then don't add scope.
//							if (!isPredicate(ss)) {
//								result += this.nextStateHelper(ss, scope);
//							} else {
//								// if source is a method declared inside the class, declaring the scope is not mandatory
//								if(entryPointNextStateParser(ss)[0].contains(c.getName()))
//									result += this.nextStateHelper(ss);
//							}
//
//						}
//					}
//				} else {
////					in the case of entry point
//					result += "----Begin EntryPoint----- \n";
//					for (String s : cm.directedGraph.vertexSet()) {
//						for (DefaultEdge ss : cm.directedGraph.outgoingEdgesOf(s)) {
//							String source = entryPointNextStateParser(ss)[0];
//							String dest = entryPointNextStateParser(ss)[1];
//
//							if (this.isEntryPoint(source.strip())) {
//								result += this.addState(source, dest);
//							} else {
//								result += this.addState("Exit" + source, dest);
//							}
//						}
//					}
//				}
//			}
//		}
//		result += "TRUE : { Exit" + this.entryPointClass + "#" + this.entryPointMethod +"}; \n" ;
//		result += "esac;";
//		return result;
//	}

	private static boolean isPredicate(DefaultEdge ss) {
		// TODO add all list of predicates
		return entryPointNextStateParser(ss)[0].contains("IF");
	}

	private static String[] entryPointNextStateParser(DefaultEdge s) {
		String temp = s.toString().replace("(", "");
		temp = temp.replace(")", "");
		return temp.toString().split(":");
	}

	/**
	 * Helper to parse a {@code Default Edge type}
	 * 
	 * @param e
	 * @param scope
	 * @return a string that contains a next state smv syntax
	 */
	private String nextStateHelper(DefaultEdge e, String scope) {
		String temp = e.toString().replace("(", "");
		temp = temp.replace(")", "");
		String[] result = temp.toString().split(":");
		return this.addState(result[0], result[1], scope);

	}

	private String nextStateHelper(DefaultEdge e) {
		String temp = e.toString().replace("(", "");
		temp = temp.replace(")", "");
		String[] result = temp.toString().split(":");
		return this.addState(result[0], result[1]);

	}

	protected String printSmvModel() {
		String result = new String();
		result += "MODULE main \n";
		result += this.getVarList() + "\n";
		result += this.getAssignInit();
		result += this.getNextState();
		result += "---- \n";
		result += this.getNextStateScope();
		result += "---- \n";

		return result;
	}

	/**
	 * 
	 * @param source
	 * @param dest
	 * @return a next state in smv syntax example: state = source : {dest};
	 */
	private String addState(String source, List<String> dest) {
		String temp = "{ ";
		for (String d : dest) {
			temp += d + ",";
		}
		temp += "};";
		return "state = " + source + " : " + temp + "\n";
	}

	private String addStateScope(String scope) {
		return "e" + scope + " = TRUE : TRUE; \n";
	}

	private String addState(String source, String dest) {
		return "state = (" + source + ") : {" + dest + "}; \n";
	}

	private String addState(String source, String dest, String scope) {
		String sc = "& e" + scope;
		return "state = (" + source + ") " + sc + " : {" + dest + "}; \n";
	}

	private String getNextState() {
		String result = new String();
		result += "next(state) := case \n";
		ArrayList<String> vt = new ArrayList<String>();
		Stack<String> STACK = new Stack<String>();
		String node;
		node = this.entryPointClass + "#" + this.entryPointMethod;
		STACK.push(this.entryPointClass + "#" + this.entryPointMethod);
		// nextstate list
		HashMap<String, ArrayList<String>> nt = new HashMap<String, ArrayList<String>>();
		this.walkusingmain(node, STACK, nt, vt);

//		result += this.walk(node, STACK);
		// TODO work on separating source;
		for (String s : nt.keySet()) {
			// Separate source from scope
			String[] t = s.split("@");
			String sourc = t[0];
			String sc = t[1];
			String dest = new String();
			for (String d : nt.get(s)) {
				dest += d + ",";
			}
			dest = dest.substring(0, dest.length() - 1);
			result += addState(sourc, dest, sc);
		}
		result += "TRUE : { Exit" + this.entryPointClass + "#" + this.entryPointMethod + "}; \n";
		result += "esac;";
		return result;
	}

	private boolean walkusingmain(String node, Stack<String> STACK, HashMap<String, ArrayList<String>> res,
			ArrayList<String> VISITED) {
		// dest[]

		String nextNode = new String();
		String prevScope = STACK.peek();
		Set<Scope> outgoingEdges = this.graph.outgoingEdgesOf(node);
		if (!outgoingEdges.isEmpty()) {
			for (Scope edge : outgoingEdges) {
				if (STACK.contains(edge.getScope())) {
					while (!STACK.peek().equals(edge.getScope())) {
						prevScope = STACK.pop();

					}
				} else {
					STACK.push(edge.getScope());
					this.addScope(STACK.peek(), prevScope);
				}

				String key = node + "@" + edge.getScope();

				if (!res.containsKey(key))
					res.put(key, new ArrayList<String>());
				res.get(key).add(edge.target());
				if (!VISITED.contains(edge.target())) {
					VISITED.add(edge.target());
					this.walkusingmain(edge.target(), STACK, res, VISITED);
				}
					
				
			}
		}

		return true;
	}

	private HashMap<String, ArrayList<String>> walk(String node, Stack<String> STACK,
			HashMap<String, ArrayList<String>> res) {
		// dest[]
		if (node.equals(("Exit" + this.entryPointClass + "#" + this.entryPointMethod)))
			return res;

		String nextNode = new String();
		String prevScope = STACK.peek();
		Set<Scope> outgoingEdges = this.graph.outgoingEdgesOf(node);
		for (Scope edge : outgoingEdges) {
			if (STACK.contains(edge.getScope())) {

				while (!STACK.peek().equals(edge.getScope())) {
					prevScope = STACK.pop();

				}
				nextNode = edge.target();
				this.addScope(STACK.peek(), prevScope);
				String key = node + "@" + edge.getScope();
				if (!res.containsKey(key)) {
					res.put(key, new ArrayList<String>());
				}
				res.get(key).add(nextNode);

			} else {
				if (edge.getScope().equals(node)) {
					nextNode = edge.target();
					STACK.push(edge.getScope());
					String key = node + "@" + edge.getScope();
					if (!res.containsKey(key)) {
						res.put(key, new ArrayList<String>());
					}
					res.get(key).add(nextNode);
					this.addScope(STACK.peek(), prevScope);

				}

			}

		}
//		HashMap<String, ArrayList<String>> go = this.walk(nextNode, STACK,res);
//		for (String gok : go.keySet()) {
//			if (res.containsKey(gok)) {
//				res.get(gok).addAll(go.get(gok));
//			} else {
//				res.put(gok, go.get(gok));
//			}
//		}
		return this.walk(nextNode, STACK, res);
	}

//	private HashMap<String, ArrayList<String>> walk(String node, Stack<String> STACK) {
//		String result = new String();
//		HashMap<String, ArrayList<String>> res = new HashMap<String, ArrayList<String>>(); // k: source+scope , v:
//																							// dest[]
//		Stack<String> scope ;
//		scope = (Stack<String>) STACK.clone();
//		if (node.equals(("Exit" + this.entryPointClass + "#" + this.entryPointMethod)))
//			return res;
//
//		String nextNode;
//		String prevScope = scope.peek();
//		Set<Scope> outgoingEdges = this.graph.outgoingEdgesOf(node);
//		for (Scope edge : outgoingEdges) {
//			Stack<String> newScope = scope;
//			if (newScope.contains(edge.getScope())) {
//
//				while (!newScope.peek().equals(edge.getScope())) {
//					prevScope = newScope.pop();
//
//				}
//				nextNode = edge.target();
//				this.addScope(newScope.peek(), prevScope);
//				result += "state = (" + node + ") &" + "e" + edge.getScope() + ": {" + nextNode + "};\n";
//				String key = node + "@" + edge.getScope();
//				if (!res.containsKey(key)) {
//					res.put(key, new ArrayList<String>());
//					ArrayList<String> temp = res.get(key);
//					temp.add(nextNode);
//					res.put(key, temp);
//					HashMap<String, ArrayList<String>> go = this.walk(nextNode, newScope);
//					for (String gok : go.keySet()) {
//						if (res.containsKey(gok)) {
//							res.get(gok).addAll(go.get(gok));
//						} else {
//							res.put(gok, go.get(gok));
//						}
//					}
//				} else {
//					ArrayList<String> temp = res.get(key);
//					temp.add(nextNode);
//					res.put(key, temp);
//					HashMap<String, ArrayList<String>> go = this.walk(nextNode, newScope);
//					for (String gok : go.keySet()) {
//						if (res.containsKey(gok)) {
//							res.get(gok).addAll(go.get(gok));
//						} else {
//							res.put(gok, go.get(gok));
//						}
//					}
//
//				}
//
//			} else {
//				if (edge.getScope().equals(node)) {
//					nextNode = edge.target();
//					newScope.push(edge.getScope());
//					result += "state = (" + node + ") &" + "e" + edge.getScope() + ": {" + nextNode + "};\n";
//					String key = node + "@" + edge.getScope();
//					if (!res.containsKey(key)) {
//						res.put(key, new ArrayList<String>());
//					}
//					ArrayList<String> temp = res.get(key);
//					temp.add(nextNode);
//					res.put(key, temp);
//					this.addScope(newScope.peek(), prevScope);
//					HashMap<String, ArrayList<String>> go = this.walk(nextNode, newScope);
//					for (String gok : go.keySet()) {
//						if (res.containsKey(gok)) {
//							res.get(gok).addAll(go.get(gok));
//						} else {
//							res.put(gok, go.get(gok));
//						}
//					}
//				}
//
//			}
//
//		}
//
//		return res;
//	}

	private void addScope(String scope, String node) {
		// find the scope of node.
		String temp = node;

		if (this.scope.containsKey(scope)) {
			this.scope.get(scope).add(temp);
		} else {
			ArrayList<String> arraylist = new ArrayList<String>();
			arraylist.add(temp);
			this.scope.put(scope, arraylist);
		}
	}

	public boolean writeFile() {
		FileWriter writer;
		try {
			writer = new FileWriter(Config.TEMPFOLDER() + "\\" + "graph" + ".smv");
			BufferedWriter bwr = new BufferedWriter(writer);
			bwr.write(this.printSmvModel());
			bwr.close();
			return true;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}

	private static void print(Object o) {
		System.out.println(o);
	}
}
