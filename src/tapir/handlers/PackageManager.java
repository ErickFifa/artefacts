package tapir.handlers;

import java.util.ArrayList;

import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.JavaModelException;

public class PackageManager {
	ICompilationUnit[] compilationUnits;
	String name;
	IPackageFragment pack;

	/**
	 * Constructor
	 * 
	 * @param p
	 * @throws Exception
	 */
	public PackageManager(IPackageFragment aPackage) throws Exception {
		this.pack = aPackage;
		this.compilationUnits = getCompilationUnit();
		this.name = this.compilationUnits.toString();
	}

	private ICompilationUnit[] getCompilationUnit() throws Exception {

		return this.pack.getCompilationUnits();
	}

	public void construct() {
		for (ICompilationUnit unit : this.compilationUnits) {
			CompUnitManager cm = new CompUnitManager(this.name, unit);
			try {
				cm.initMethodClass();
			} catch (JavaModelException e) {

				e.printStackTrace();
			}
		}
	}

	public ArrayList<CompUnitManager> getCompUnit() {
		ArrayList<CompUnitManager> list = new ArrayList<CompUnitManager>();
		for (ICompilationUnit unit : this.compilationUnits) {
			CompUnitManager cm = new CompUnitManager(this.name, unit);
			list.add(cm);
		}
		
		return list;
	}

	public String getList() {
		String res = "";
		for (ICompilationUnit unit : this.compilationUnits) {
			res += unit.getElementName();
		}
		return res;
	}
}
