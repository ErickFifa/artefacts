/**
 * 
 */
package tapir.handlers;

import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;

import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaModelException;
import org.jgrapht.graph.DefaultDirectedGraph;

/**
 * @author erick This class get all the project parsing
 */
public class ProjectParser {
	String Name;
	ArrayList<Classe> className;
	DeepGraph deepGraph;
	NuXmvModel model;
	ModelChecker checker;
	String epC,epM;
	private ArrayList<Asset> assets;
	private ArrayList<String> specs;

	public ProjectParser() throws JavaModelException {
		
		Config.setAPIList("");
		if (Config.isSet()) {
			this.className = new ArrayList<Classe>();
			this.Name = new AstFetcher().getProjectName();
//			this.fetchClass();
			this.newClassFetcher();
			// TODO remove the entry point Main#main hard code.
			this.deepGraph = new DeepGraph(this.className, "Main", "main");
			this.model = new NuXmvModel(this.className, "Main", "main", this.deepGraph.getGraph());
			if (this.model.writeFile())
				System.out.println("Model generated");
			this.specs = getSpecs();
			this.checker = new ModelChecker();
			System.out.println("Inting Model");
			this.checker.initModel();
			System.out.println("Fetching asset");
			this.assets = getAssets();
			System.out.println("Done Fetching asset");
			this.deepGraph.exportGraph(this.APIMethodCaller());
			
		} else {
			System.err.print("config file not set");
		}

	}
	
	public ProjectParser(String classe, String meth) throws JavaModelException {
		long startTime = System.currentTimeMillis();
		Config.setAPIList("");
		this.epC = classe;
		this.epM = meth;
		if (Config.isSet()) {
			this.className = new ArrayList<Classe>();
			this.Name = new AstFetcher().getProjectName();
//			this.fetchClass();
			this.newClassFetcher();
			// TODO remove the entry point Main#main hard code.
			this.deepGraph = new DeepGraph(this.className, classe, meth);
			long endTime = System.currentTimeMillis();

			long timeElapsed = endTime - startTime;
			System.err.println("DG gen: " + timeElapsed);
			startTime = System.currentTimeMillis();
			this.model = new NuXmvModel(this.className,classe,meth, this.deepGraph.getGraph());
			if (this.model.writeFile())
			this.specs = getSpecs();
			this.checker = new ModelChecker();
			this.checker.initModel();
			this.assets = getAssets();
			this.deepGraph.exportGraph(this.APIMethodCaller());
			 endTime = System.currentTimeMillis();

			 timeElapsed = endTime - startTime;
			System.err.println("Model Checking time: " + timeElapsed);
		} else {
			System.err.print("config file not set");
		}

	}

	/**
	 * 
	 * @return a list of method that contains API method call
	 */
	private ArrayList<String> APIMethodCaller() {
		ArrayList<String> res = new ArrayList<String>();
		for (Classe c : this.className) {
			for (Method m : c.methods) {
				if (m.containsAPICall())
					res.add(m.getName());
			}
		}

		return res;
	}

	public void exportAPIMethodCaller() {
		try {
			Path out = Paths.get(Config.TEMPFOLDER()+ "\\method.list");
			Files.write(out, this.APIMethodCaller(), Charset.defaultCharset());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Constructor to export visual of the project
	 * 
	 * @param draw
	 * @throws JavaModelException
	 */
	public ProjectParser(boolean draw) throws JavaModelException {
		if (Config.isSet()) {
			this.className = new ArrayList<Classe>();
			this.Name = new AstFetcher().getProjectName();
//			this.fetchClass();
			this.newClassFetcher();
			// TODO remove the entry point Main#main hard code.
			this.deepGraph = new DeepGraph(this.className, "Main", "main");
			this.specs = getSpecs();
			this.checker = new ModelChecker();
			this.checker.initModel();
			this.assets = getAssets();

			this.deepGraph.exportGraph();
		} else {
			System.err.print("config file not set");
		}

	}

	public void refresh() {
		if (Config.isSet()) {
			this.className = new ArrayList<Classe>();
			this.Name = new AstFetcher().getProjectName();
			try {
				this.newClassFetcher();
			} catch (Exception e) {
				e.printStackTrace();
			}

			// TODO remove the entry point Main#main hard code.
			this.deepGraph = new DeepGraph(this.className, this.epC, this.epM);
			this.model = new NuXmvModel(this.className, this.epC, this.epM, this.deepGraph.getGraph());
			if (this.model.writeFile())
				System.out.println("Model re-generated");
			this.specs = getSpecs();
			this.checker = new ModelChecker();
			this.checker.initModel();
			this.assets = getAssets();
			this.deepGraph.exportGraph();
		} else {
			System.err.print("config file not set");
		}
	}

	public static ArrayList<String> getSpecs() {
		try {
//			String ltlFile = Configuration.tapirConfiguration[2];
			String ltlFile = Config.LTLFILE();
			System.out.println("Ltl File: " + ltlFile);
			return new LtlParser(new FileReader(ltlFile)).getSpecList();
		} catch (Exception e) {
			System.err.print("Spec File not Found");
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 
	 * @return a lsit of asset
	 */
	private ArrayList<Asset> getAssets() {
		// loop through the spec and build the assets
		ArrayList<Asset> list = new ArrayList<Asset>();
		int id = 0;
		for (String spec : this.specs) {
			String answer = new String();
			answer = this.checker.checkSpec(spec);
			list.add(new Asset(id, spec, answer));
			id++;
		}

		return list;
	}

	/**
	 * used by the view class {@link tapir.views}(UI)
	 * 
	 * @return list of asset
	 */
	public ArrayList<Asset> getViewElement() {
		return this.assets;
	}

//	private void fetchClass() throws JavaModelException {
//		ArrayList<CompUnitManager> cus = new AstFetcher().getPackageManager().get(0).getCompUnit();
//
//		for (CompUnitManager cu : cus) {
//			for (IType iType : cu.classList) {
//				String classname = iType.getElementName();
//				String source = iType.getSource();
//				this.className.add(new Classe(classname, source));
//			}
//
//		}
//	}

	/**
	 * get all class name inside the active eclipse project (Doesn't take into
	 * account nested class )
	 * 
	 * @throws JavaModelException
	 */
	private void newClassFetcher() throws JavaModelException {
		ArrayList<ICompilationUnit> list = new AstFetcher().getAllCompUnit();
		for (ICompilationUnit cu : list) {
			for (IType it : cu.getAllTypes()) {
				String classname = it.getElementName();
				String source = it.getSource();
				Classe c = new Classe(classname, source);
				this.className.add(c);
			}
		}
	}

	public HashMap<Integer, String> listOfViolatedPattern() {
		HashMap<Integer, String> res = new HashMap<Integer, String>();
		for (Asset a : this.assets) {
			if (a.isFalse()) {
				res.put(a.id, a.getPattern());
			}
		}
		return res;
	}

	public void exportCounterExample() {
		HashMap<Integer, String> vps = this.listOfViolatedPattern();
		for (int vp : vps.keySet()) {
			ArrayList<String> instruct = this.checker.checkSpec(vps.get(vp), true);
			DefaultDirectedGraph<String, Scope> tempg = this.deepGraph.counterExample(instruct);

			DeepGraph.exportGraph(Integer.toString(vp), tempg, instruct);

		}

	}

	/**
	 * a Print method for debugging purpose
	 */
	public void print() {
//		System.out.print(this.model.printSmvModel());
//		System.out.println(this.className.get(0).methods.get(2).getSmvModel());
//		System.out.println(this.deepGraph.graph.toString());
//		System.out.println(this.model.writeFile());
	}

}
