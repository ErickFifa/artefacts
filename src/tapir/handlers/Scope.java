package tapir.handlers;

import org.jgrapht.graph.DefaultEdge;

public class Scope extends DefaultEdge {
	private String scope;
	
	public Scope(String s) {
		this.scope = s;
	}
	
	
	public String getScope() {
		return scope;
	}
	
	public String target() {
		return getTarget().toString();
	}
	
	public String source() {
		return getSource().toString();
	}
	
	@Override
	public String toString() {
		return scope ;
	}
}
