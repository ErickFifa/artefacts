package tapir.handlers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Specification {
	String formula;
	List<String> variable = new ArrayList<String>();
	public Specification(String formula, List<String> vars) {
		this.formula = formula;
		this.variable = vars;
	}
	
	public String printMaster() {
		return "the raw formula is: "+this.formula;
	}
	public String getVariable() {
		String res = new String();
		Iterator it = this.variable.iterator();
		while(it.hasNext()) {
			res += it.next();
		}
		return res;
	}
}
