package tapir.handlers;

import java.io.IOException;
import java.util.ArrayList;

import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.JavaModelException;

public class TypeManager {
	public IMethod[] methodList;
	String source;

	public TypeManager(IMethod[] methods, String from) {
		this.methodList = methods;
		this.source = from;
	}


	
	public ArrayList<MethodManager> getMethodManager() {
		ArrayList<MethodManager> list = new ArrayList<MethodManager>();
		for (IMethod method : this.methodList) {
			MethodManager mm = new MethodManager(method);
			list.add(mm);
		}
		return list;
	}
	
}
