package tapir.handlers;

import java.io.FileWriter;
import java.io.IOException;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;

import org.eclipse.jdt.core.JavaModelException;

import tapir.views.Configuration;
 import tapir.handlers.CallGraph;
public class Wrapper {
	AstFetcher ast;
	ArrayList<PackageManager> pack;
	ArrayList<CompUnitManager> cu;
	ArrayList<TypeManager> tm;
	ArrayList<MethodManager> mm;
	ArrayList<Graph> graphs;
	ArrayList<String> methodName; 
	ArrayList<String> specs;
	ArrayList<Asset> assets;
	ModelChecker modelCheker;
	String entryPoint;

	public Wrapper(int id) {
		try {
			this.entryPoint = "MainClass.java";
			this.ast = new AstFetcher();
			this.pack = this.ast.getPackageManager();
			this.cu = this.pack.get(0).getCompUnit();
			System.out.println("EntryPoint in Intra Constructor= "+ this.entryPoint);
			int indexOfEntryPoint = this.cu.indexOf(this.getEntryPoint(this.entryPoint));
			this.tm = this.cu.get(indexOfEntryPoint).getTypeManager();
			this.mm = this.tm.get(0).getMethodManager();

			this.intraBuildGraph();
			this.specs = getSpecs();
			this.modelCheker = new ModelChecker();
			this.assets = this.getAssets(id);
		} catch (Exception e) {
			System.err.println("error generating the intra agent");
			e.printStackTrace();
		}

	}
	public Wrapper() {
		try {
			this.entryPoint = "MainClass.java";
			this.ast = new AstFetcher();
			this.pack = this.ast.getPackageManager();
			this.cu = this.pack.get(0).getCompUnit();
			this.tm = this.cu.get(0).getTypeManager();
			System.out.print("OK");
			this.mm = this.tm.get(0).getMethodManager();
			this.interBuildGraph();
			this.specs = getSpecs();
			this.modelCheker = new ModelChecker();
			this.assets = this.getAssets();
		} catch (Exception e) {
			System.err.println("error generating the agent");
			e.printStackTrace();
		}

	}
	
	private CompUnitManager getEntryPoint(String className) {
		
		for(CompUnitManager c : this.cu) {
			if(c.cUnit.getElementName().contains(className)) {
				System.out.print("$$$YES");
				return c;
			}
			
		}
		return null;
	}
	public void buildGraph(int id) throws JavaModelException {
		System.out.println("Name of the Method " + this.mm.get(id).getName());
		Graph g = new Graph(this.mm.get(id).getSource());
		this.graphs.add(g);

		g.printGraph();

	}

	public void intraBuildGraph() {
		try {
			this.graphs = new ArrayList<Graph>();
			this.methodName = new ArrayList<String>();
			for (MethodManager m : this.mm) {
				this.methodName.add(m.getName());
				this.graphs.add(new Graph(m.getSource()));
			}
			this.assets = this.getAssets(0);
//			System.out.println("g:" + this.graphs.get(0).printDico());
		} catch (Exception e) {
			System.err.print("Agent: java model generation failed: " + e);
		}

	}
	public void interBuildGraph() {
		try {
			this.graphs = new ArrayList<Graph>();
			this.methodName = new ArrayList<String>();
			for (MethodManager m : this.mm) {
				this.methodName.add(m.getName());
				this.graphs.add(new Graph(m.getSource()));
				
			}
			this.assets = this.getAssets(0);
//			this.checkMethodCall();
//			System.out.println("g:" + this.graphs.get(0).printDico());
		} catch (Exception e) {
			System.err.print("Agent: java model generation failed: " + e);
		}

	}

	public void checkMethodCall() {

		ArrayList<ArrayList<String>> list = new ArrayList<ArrayList<String>>();
		// For each graph
		for (Graph g : this.graphs) {
			ArrayList<String> nodeName = new ArrayList<String>();
			// get in alist of name of each node
			for (Node n : g.nodes) {
				nodeName.add(n.getLabel());
			}
			list.add(nodeName);
		}
		

		for (ArrayList<String> nn : list) {
			System.out.println("===========");
			for (int i = 0; i < this.methodName.size(); i++) {
				if (nn.contains(this.methodName.get(i))) {
					System.out.println(
							"InterProcedural call found in " + list.indexOf(nn) + " -> " + this.methodName.get(i));
					Graph g = mergeGraph(this.graphs.get(list.indexOf(nn)), this.graphs.get(i),this.methodName.get(i));
					this.graphs.add(g);
				}
			}

		}

	}

	/**
	 * 
	 * @param First  graph
	 * @param second graph to be merged inside the second graph
	 * @param name  of the node where the graph should be merged
	 * @return the merged graph
	 */
	private Graph mergeGraph(Graph a, Graph b, String nodeName) {
		System.out.println("======Node name = ====" + nodeName);
		Graph big = new Graph();
		int i = 0;
		// Loop through graph a node
		for (Node n : a.nodes) {
			if (n.getLabel().equals(nodeName)) {
				
				System.out.println("Inside the nested loop");
				for (Node na : b.nodes) {
					System.out.println("FEtching inside node");
					
					if(na.isEntry())
						continue;
					if(na.isExit())
						continue;
					i++;
					big.nodes.add(na);
					big.addEdge(i-1, i);
				}
			}else {
				if(n.isExit())
					continue;
				i++;
				big.nodes.add(n);
				big.addEdge(i-1, i);
				System.out.println("label|"+n.getLabel()+"|");
				
			}
			
			
		}
		big.nodes.add(new Node(big.getLastNode(), "ExitPoint", 0, 0));
//		big.addEdge(big.getLastNode()-1, big.getLastNode() );
		big.addEdge(big.getLastNode(), big.getLastNode());
//		big.addEdge(big.getLastNode()-1, big.getLastNode());
//		big.addEdge(big.getLastNode(), big.getLastNode());
		System.out.println("a = "+ a.printDico());
		System.out.println("===========");
		System.out.println("b = "+ b.printDico());
		System.out.println("===========");
		System.out.println("big = "+ big.printDico());

		return big;
	}

	public void interRefreshGraph() {
		try {
			this.ast = new AstFetcher();
			this.pack = this.ast.getPackageManager();
			this.cu = this.pack.get(0).getCompUnit();
			this.tm = this.cu.get(0).getTypeManager();
			this.mm = this.tm.get(0).getMethodManager();
			System.out.println("GM OK");
			this.interBuildGraph();
			System.out.println("Build OK");
			this.specs = getSpecs();
			this.modelCheker = new ModelChecker();
			this.assets = this.getAssets();
		
			
		} catch (Exception e) {
			System.err.print("error Refreshing inter: " );
			e.printStackTrace();
		}

	}
	public void intraRefreshGraph(String s) {
		try {
			this.entryPoint = s;
			System.out.println("EntryPoint in IntraRefresh= "+ this.entryPoint);
//			this.tm = this.cu.indexOf(this.getEntryPoint(this.entryPoint)).getTypeManager();
			int indexOfEntryPoint = this.cu.indexOf(this.getEntryPoint(this.entryPoint));
			this.tm = this.cu.get(indexOfEntryPoint).getTypeManager();
			this.mm = this.tm.get(0).getMethodManager();
			this.intraBuildGraph();
			this.specs = getSpecs();
			this.modelCheker = new ModelChecker();
			this.assets = this.getAssets(0);
		} catch (Exception e) {
			System.err.print("error Refreshing intra");
			e.printStackTrace();
			System.err.print("=============");
		}

	}

	public ArrayList<String> getSpecs() {
		try {
//			String ltlFile = Configuration.tapirConfiguration[2];
			String ltlFile = Config.LTLFILE();
			System.out.println("Ltl File: " +ltlFile);
			return new LtlParser(new FileReader(ltlFile))
					.getSpecList();
		} catch (Exception e) {
			System.err.print("Spec File not Found");
			e.printStackTrace();
		}
		return null;
	}

	private ArrayList<Asset> getAssets(int i) throws IOException {
		// loop through the spec and build the assets
		ArrayList<Asset> list = new ArrayList<Asset>();
		int id = 0;
		for (String spec : this.specs) {
			System.out.println("for intra:-=-=-=-=-=-=-=-=-");
			String answer = this.modelCheck(0, spec);
		
			list.add(new Asset(id, spec, answer));
			id++;
		}

		return list;
	}
	private ArrayList<Asset> getAssets() throws IOException {
		// loop through the spec and build the assets
		ArrayList<Asset> list = new ArrayList<Asset>();
		int id = 0;
		System.out.println("This specs element:" + this.specs);
		for (String spec : this.specs) {
			System.out.println("for inter:-=-=-=-=-=-=-=-=-");
			String answer = this.modelCheck(this.graphs.size()-1, spec);

			list.add(new Asset(id, spec, answer));
			id++;
		}

		return list;
	}

	public ArrayList<Asset> getViewElement() {
		return this.assets;
	}

	/**
	 * 
	 * @param i    index of the graph
	 * @param spec the ltl pattern in a string
	 * @return true if the model respect the spec otherwise false.
	 * @throws IOException
	 */
	private String modelCheck(int i, String spec) throws IOException {
		// write the file (graph ) as input
		//Get Path to temporary folder from the configuration in config.tapriconf[0]
//		String temporaryFolder = Configuration.tapirConfiguration[0];
		String temporaryFolder = Config.TEMPFOLDER();
		File file = new File(temporaryFolder + "\\graph.smv");
//		System.out.println("file location: " + System.getProperty("user.dir") + "\\resources\\" + "graph.smv");
		try {
			String smvModel = "";
			for (String str : this.graphs.get(i).getSmvConf()) {
				smvModel += str + "\n";
			}
			// add the pattern inside the smv model
			smvModel += spec;
			System.out.println("---This is Model:::: " + smvModel);
			BufferedWriter writer = new BufferedWriter(new FileWriter(file));
			writer.write(smvModel);
			writer.close();
//			System.out.println("args: "+ this.modelCheker.args);
			this.modelCheker.initModel();
//			System.out.println("---This is output in wrapper:::: " + this.modelCheker.output);
			return this.modelCheker.getAnswer();

		} catch (Exception e) {
			System.err.print("Error runing the model Checker program");
			e.printStackTrace();
		}
		return "Undefined";
	}

}
