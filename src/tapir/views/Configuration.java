package tapir.views;

import java.io.IOException;
import java.util.ArrayList;

import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Link;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.part.ViewPart;

public class Configuration extends ViewPart {

	public static final String ID = "tapir.views.Configuration"; //$NON-NLS-1$
	public static String[] tapirConfiguration = new String[3];
	 
	public Configuration() {
		 
	}

	/**
	 * Create contents of the view part.
	 * 
	 * @param parent
	 */
	@Override
	public void createPartControl(Composite parent) {

		Composite top = new Composite(parent, SWT.NONE);// embedded Composite

		// setup the layout of top to be GridLayout.
		GridLayout layout = new GridLayout();
		layout.marginHeight = 0;
		layout.marginWidth = 0;
		top.setLayout(layout);

		// top banner
		Composite banner = new Composite(top, SWT.NONE);// banner is added to
														// "top"
		banner.setLayoutData(
				new GridData(GridData.HORIZONTAL_ALIGN_FILL, GridData.VERTICAL_ALIGN_BEGINNING, true, false));
		layout = new GridLayout();
		layout.marginHeight = 5;
		layout.marginWidth = 10;

		layout.numColumns = 3;
		banner.setLayout(layout);

		// setup bold font
		Font boldFont = JFaceResources.getFontRegistry().getBold(JFaceResources.DEFAULT_FONT);

		GridData gridData = new GridData();
		gridData.horizontalAlignment = SWT.FILL;
		gridData.minimumWidth = 400;
		gridData.minimumHeight = 50;
		gridData.grabExcessHorizontalSpace = true;

		// 1st row -------------------------------
		Label l = new Label(banner, SWT.WRAP);
		l.setText("Select a temporary folder:");
		l.setFont(boldFont);

		final DocChooser tempChooser = new DocChooser(banner);
		gridData.heightHint = 25;
		tempChooser.setLayoutData(gridData);
		// fileChooser.setLayout(SWT.WRAP);

		Button tempButton = new Button(banner, SWT.HORIZONTAL);
		tempButton.setText("Set temporary Folder");
		
		// message contents
	

		tempButton.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {

				
				tapirConfiguration[0] = tempChooser.getText();
				
				
			}
		});
		// 2nd row -------------------------------

		Label mcLabel = new Label(banner, SWT.PUSH);
		mcLabel.setText("Select NuXmv root folder:");
		mcLabel.setFont(boldFont);

		final DocChooser mcChooser = new DocChooser(banner);
		gridData.heightHint = 25;
		mcChooser.setLayoutData(gridData);
		// fileChooser.setLayout(SWT.WRAP);

		Button mcButton = new Button(banner, SWT.PUSH);
		mcButton.setText("Set NuXmv Folder");

		// message contents

		// here like the banner, text is added to "top".
		

		mcButton.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {

				
				tapirConfiguration[1] = mcChooser.getText();
				
				System.out.print(tapirConfiguration[1]);

			}
		});
		// 3rd row -------------------------------

		Label ltlLabel = new Label(banner, SWT.PUSH);
		ltlLabel.setText("Select LTL configuration File:");
		ltlLabel.setFont(boldFont);

		final FileChooser ltlChooser = new FileChooser(banner);
		gridData.heightHint = 50;
		ltlChooser.setLayoutData(gridData);
		// fileChooser.setLayout(SWT.WRAP);

		Button ltlButton = new Button(banner, SWT.PUSH);
		ltlButton.setText("Set LTL File");

		// message contents

		// here like the banner, text is added to "top".
		

		ltlButton.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {

				tapirConfiguration[2] = ltlChooser.getText();
				
				System.out.print(tapirConfiguration[2]);
			}
		});
		createActions();
		initializeToolBar();
		initializeMenu();
	}
	
	public String[] returnConfiguration() {
		return tapirConfiguration;
	}
	/**
	 * Create the actions.
	 */
	private void createActions() {
		// Create the actions
	}
	
	/**
	 * Initialize the toolbar.
	 */
	private void initializeToolBar() {
		IToolBarManager toolbarManager = getViewSite().getActionBars().getToolBarManager();
	}

	/**
	 * Initialize the menu.
	 */
	private void initializeMenu() {
		IMenuManager menuManager = getViewSite().getActionBars().getMenuManager();
	}

	@Override
	public void setFocus() {
		// Set the focus
	}

}
