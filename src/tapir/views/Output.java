package tapir.views;

import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.IResourceChangeListener;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;

import tapir.handlers.Asset;
import tapir.handlers.Wrapper;

import org.eclipse.swt.widgets.Table;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.layout.TableColumnLayout;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.ColumnPixelData;
import org.eclipse.swt.widgets.Button;

public class Output extends ViewPart {

	public static final String ID = "tapir.views.Output"; //$NON-NLS-1$
	private Table table;
	private TableViewer tableViewer;
	Wrapper agent;
	IResourceChangeListener listener = new IResourceChangeListener() {

		@Override
		public void resourceChanged(IResourceChangeEvent event) {
			if(agent != null) {
				String s =PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActiveEditor().getTitle();
				System.out.print("active editor == " + s);
				agent.intraRefreshGraph(s);
			}
			
			Display.getDefault().syncExec(new Runnable() {
				public void run() {
					tableViewer.refresh();

					tableViewer.setInput(getSpec());

				}
			});
			
		}
		
	};
	public Output() {
	
		
		this.agent = new Wrapper(0);
	}

	/**
	 * Create contents of the view part.
	 * 
	 * @param parent
	 */
	@Override
	public void createPartControl(Composite parent) {

		Composite composite = new Composite(parent, SWT.NONE);
		TableColumnLayout tcl_composite = new TableColumnLayout();
		composite.setLayout(tcl_composite);

		tableViewer = new TableViewer(composite, SWT.BORDER | SWT.FULL_SELECTION);
		table = tableViewer.getTable();
		table.setHeaderVisible(true);
		table.setLinesVisible(true);

		TableViewerColumn idC = new TableViewerColumn(tableViewer, SWT.NONE);
		TableColumn columnID = idC.getColumn();
		tcl_composite.setColumnData(columnID, new ColumnPixelData(150, true, true));
		columnID.setText("ID");
		idC.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				Asset as = (Asset) element;
				return as.getId();
			}
		});
		TableViewerColumn patC = new TableViewerColumn(tableViewer, SWT.NONE);
		TableColumn columnPattern = patC.getColumn();
		tcl_composite.setColumnData(columnPattern, new ColumnPixelData(150, true, true));
		columnPattern.setText("Pattern");
		patC.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				Asset as = (Asset) element;
				return as.getPattern();
			}
		});
		TableViewerColumn resC = new TableViewerColumn(tableViewer, SWT.NONE);
		TableColumn columnResult = resC.getColumn();
		tcl_composite.setColumnData(columnResult, new ColumnPixelData(150, true, true));
		columnResult.setText("Result");
		resC.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				Asset as = (Asset) element;
				return as.getResult();
			}
		});
		TableViewerColumn descC = new TableViewerColumn(tableViewer, SWT.NONE);
		TableColumn columnDescription = descC.getColumn();
		tcl_composite.setColumnData(columnDescription, new ColumnPixelData(150, true, true));
		columnDescription.setText("Description");
		descC.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				Asset as = (Asset) element;
				return "";
			}
		});
		tableViewer.setContentProvider(new ArrayContentProvider());
		ResourcesPlugin.getWorkspace().addResourceChangeListener(listener);
		getEntries();
		initializeToolBar();
		initializeMenu();
	}

	/**
	 * Create the actions.
	 */
	private void getEntries() {
		tableViewer.refresh();
		tableViewer.setInput(getSpec());
	}

	private Object getSpec() {
		return agent.getViewElement();
	}

	/**
	 * Initialize the toolbar.
	 */
	private void initializeToolBar() {
		IToolBarManager toolbarManager = getViewSite().getActionBars().getToolBarManager();
	}

	/**
	 * Initialize the menu.
	 */
	private void initializeMenu() {
		IMenuManager menuManager = getViewSite().getActionBars().getMenuManager();
	}

	@Override
	public void setFocus() {
		// Set the focus
	}

}
