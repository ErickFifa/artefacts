package tapir.views;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.part.*;

import tapir.handlers.Asset;
import tapir.handlers.CallGraphManager;
import tapir.handlers.ProjectParser;
import tapir.handlers.Wrapper;

import org.eclipse.jface.viewers.*;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.IResourceChangeListener;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jface.action.*;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.layout.TableColumnLayout;
import org.eclipse.ui.*;
import org.eclipse.ui.console.MessageConsole;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Widget;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyleRange;

import java.util.ArrayList;

import javax.inject.Inject;

public class View extends ViewPart {

	public static final String ID = "tapir.views.View"; //$NON-NLS-1$
	private Table table;
	private TableViewer tableViewer;
	private static ProjectParser agent;
	IResourceChangeListener listener = new IResourceChangeListener() {

		@Override
		public void resourceChanged(IResourceChangeEvent event) {
			if(agent != null) {
				agent.refresh();
			}
		
			Display.getDefault().syncExec(new Runnable() {
				public void run() {
					tableViewer.refresh();

					tableViewer.setInput(getSpec());

				}
			});
			
		}
		
	};
	public View() {
		try {
//			agent = null;
			agent= new ProjectParser("MainMap","main");
//			agent= new ProjectParser("PermissionUtils","permissions");
		}catch(Exception e) {
			e.printStackTrace();
		}
		
	}

	/**
	 * Create contents of the view part.
	 * 
	 * @param parent
	 */
	@Override
	public void createPartControl(Composite parent) {

		Composite composite = new Composite(parent, SWT.NONE);
		TableColumnLayout tcl_composite = new TableColumnLayout();
		composite.setLayout(tcl_composite);

		tableViewer = new TableViewer(composite, SWT.BORDER | SWT.FULL_SELECTION);
		table = tableViewer.getTable();
		table.setHeaderVisible(true);
		table.setLinesVisible(true);

		TableViewerColumn idC = new TableViewerColumn(tableViewer, SWT.NONE);
		TableColumn columnID = idC.getColumn();
		tcl_composite.setColumnData(columnID, new ColumnPixelData(150, true, true));
		columnID.setText("ID");
		idC.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				Asset as = (Asset) element;
				return as.getId();
			}
		});
		TableViewerColumn patC = new TableViewerColumn(tableViewer, SWT.NONE);
		TableColumn columnPattern = patC.getColumn();
		tcl_composite.setColumnData(columnPattern, new ColumnPixelData(150, true, true));
		columnPattern.setText("Pattern");
		patC.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				Asset as = (Asset) element;
				return as.getPattern();
			}
		});
		TableViewerColumn resC = new TableViewerColumn(tableViewer, SWT.NONE);
		TableColumn columnResult = resC.getColumn();
		tcl_composite.setColumnData(columnResult, new ColumnPixelData(150, true, true));
		columnResult.setText("Result");
		resC.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				Asset as = (Asset) element;
				return as.getResult();
			}
		});
		TableViewerColumn descC = new TableViewerColumn(tableViewer, SWT.NONE);
		TableColumn columnDescription = descC.getColumn();
		tcl_composite.setColumnData(columnDescription, new ColumnPixelData(150, true, true));
		columnDescription.setText("Description");
		descC.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				Asset as = (Asset) element;
				return "";
			}
		});
		tableViewer.setContentProvider(new ArrayContentProvider());
		ResourcesPlugin.getWorkspace().addResourceChangeListener(listener);
		getEntries();
		initializeToolBar();
		initializeMenu();
	}

	/**
	 * Create the actions.
	 */
	private void getEntries() {
		tableViewer.refresh();
		tableViewer.setInput(getSpec());
	}

	private Object getSpec() {
		return agent.getViewElement();
	}

	/**
	 * Initialize the toolbar.
	 */
	private void initializeToolBar() {
		IToolBarManager toolbarManager = getViewSite().getActionBars().getToolBarManager();
	}

	/**
	 * Initialize the menu.
	 */
	private void initializeMenu() {
		IMenuManager menuManager = getViewSite().getActionBars().getMenuManager();
	}

	@Override
	public void setFocus() {
		// Set the focus
	}

}
